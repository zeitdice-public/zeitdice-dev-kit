#!/usr/bin/env python3

from glob import glob
import os
import sys
import uuid
import time
import datetime
import uuid
import requests
import hashlib
import subprocess
import RPi.GPIO as GPIO
from datetime import timedelta
import urllib.request
import shutil
import psutil
import tarfile

OPERATOR_VERSION = 1
mode = 0 # Default to Config Mode
connection = "LTE"
demo_mode = 0


def logging (String):
  try: 
    with open('/proc/uptime', 'r') as f:
      uptime_seconds = float(f.readline().split()[0])
      uptime_string = str(timedelta(seconds = uptime_seconds))
    log = ('OP ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' / ' +uptime_string + ' / ' + String +'\n')
  except:
    log = ('OP ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' / ' + String +'\n')
  print(log)
  with open('/home/pi/static/log.txt','a') as file:
      file.write(log)

def rescuebot_enable():
  with open('/etc/cron.d/rescuebot', 'w') as the_file:
    the_file.write('* * * * * root /home/pi/current/rescuebot.py >> /var/log/rescuebot.log\n\n')

def rescuebot_disable():
  try:
    os.remove('/etc/cron.d/rescuebot')
  except:
    return

def get_file_hash_sha256(file_path):
    file_hash = None
    with open(file_path, "rb") as f:
        bytes = f.read()
        file_hash = hashlib.sha256(bytes).hexdigest()
    return file_hash

def download_new_firmware(firmware_url, expected_file_hash): 

  proposed_tar_path = '/home/pi/proposed.tar.gz'

  urllib.request.urlretrieve(firmware_url, proposed_tar_path)
 
  downloaded_file_hash = get_file_hash_sha256(proposed_tar_path)
  print("The downloaded hash: " + downloaded_file_hash)

  if downloaded_file_hash != expected_file_hash:
    logging("Aborting download, file hashes are not equivalent.")

  shutil.rmtree('/home/pi/proposed', ignore_errors=True)

  with tarfile.open(proposed_tar_path, 'r') as t:
    t.extractall('/home/pi/proposed')
  os.remove(proposed_tar_path)
  # on next reboot, updater will work its magic

time.sleep(3) # Give the system some time

# Define UUID based on HW in case there is none yet
if os.path.isfile('/home/pi/static/uuid.txt'):
  logging(String ="We got a uuid already")
  with open("/home/pi/static/uuid.txt","r") as f:
    device_uuid =f.read()
else: 
  device_uuid = str(uuid.getnode())
  device_uuid = hashlib.md5(device_uuid.encode('utf-8')).hexdigest()
  with open("/home/pi/static/uuid.txt","w+") as f:
    f.write(device_uuid)
  logging(String ="New UUID was generated")

logging(String = str(device_uuid))

# Connect LTE and call home 
try:
  subprocess.call(["sudo","pon"])
except:
  logging(String = "ERROR: Could not connect to LTE for some reason")
time.sleep(3) # Give the system some time
try:
 subprocess.call(["sudo", "route", "del", "default"])
 subprocess.call(["sudo", "route", "add", "default", "ppp0"])
except:
 logging(String = "ERROR: Could not change routes for some reason")

logging(String = "Should be online via LTE now, unless error shows above")

memory_size = str(psutil.disk_usage('/')[0]/(1024.0 ** 2))
memory_free = str(psutil.disk_usage('/')[2]/(1024.0 ** 2))

host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'
headers = {'Content-Type':'application/json'}

payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","operator_version":"%s","battery_level":"0","sdSizeMB":"%s","sdFreeMB":"%s"}}' % (OPERATOR_VERSION, memory_size, memory_free)

status = 0
try:
  # TODO Remove verify=False, not sure why certificate fails at this point (May 2019)
  r = requests.post(host_url, data=payload, headers=headers, verify=False)
  status = r.status_code
  logging(String ="Sent Payload")
except:
  logging(String ="ERROR: Could not send payload, either server down or we are not online")

if status == 201:
  data = r.json()
  logging(String = str(data))
  if data:
    logging(String="there was data coming back from API")
    # Download this file, override local capture.py
    if 'fw_data' in data:
      logging(String="looks like new FW to install is available")
      fw_url = data['fw_data']['url']
      fw_checksum = data['fw_data']['checksum']
      logging(String=str(fw_url))
      try:
        download_new_firmware(fw_url, fw_checksum)
      except Exception as e:
        print('An exception occurred while downloading and extracting new firmware: {}'.format(e))
  else: 
    logging(String = "No data came back from request")
  message = data['message']
  logging(String = str(message))

GPIO.setmode(GPIO.BOARD)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
mode = GPIO.input(13)
GPIO.cleanup()
subprocess.call(["sudo", "killall", "wpa_supplicant"])
if mode == 0: # Config Mode
  logging(String = "Config Mode")
  rescuebot_disable()
  subprocess.call(["sudo","cp", "/home/pi/current/config-mode-dhcpcd.conf", "/etc/dhcpcd.conf"])
  subprocess.call(["sudo","cp", "/home/pi/current/config-mode-wpa-supplicant.conf", "/etc/wpa_supplicant/wpa_supplicant.conf"])
  subprocess.call(["sudo", "wpa_supplicant", "-B", "-c/etc/wpa_supplicant/wpa_supplicant.conf", "-iwlan0", "-Dnl80211,wext"])
  subprocess.call(["sudo","systemctl", "start", "dnsmasq"])
  subprocess.call(["sudo","systemctl", "start", "networking"])
  subprocess.call(["sudo","systemctl", "start", "dhcpcd"])
  if demo_mode == 1: # Demo Mode
    try:
      subprocess.call(["sudo","python3", "/home/pi/current/demo.py"])
      logging(String = "Running demo.py")
    except:
      logging(String = "ERROR: demo.py")
  try:
    subprocess.call(["sudo","node", "/home/pi/current/ap-interface/index.js"])
  except:
    logging(String = "ERROR: Could not start node server, probably already running, if you have started operator.py manually")

if mode == 1: # Capture Mode
  logging(String = "Capture Mode")
  rescuebot_enable()
  # TODO Need to read from config file if we are using networking via WIFI or LTE
  # WIFI Connectivity
  if connection == "WIFI": 
    subprocess.call(["sudo","cp", "/home/pi/current/capture-mode-dhcpcd.conf", "/etc/dhcpcd.conf"])
    subprocess.call(["sudo","cp", "/home/pi/current/capture-mode-wpa-supplicant.conf", "/etc/wpa_supplicant/wpa_supplicant.conf"])
    subprocess.call(["sudo", "wpa_supplicant", "-B", "-c/etc/wpa_supplicant/wpa_supplicant.conf", "-iwlan0", "-Dnl80211,wext"])
    subprocess.call(["sudo","wpa_cli", "-i", "wlan0", "reconfigure"])
    subprocess.call(["sudo","systemctl", "start", "networking"])
    subprocess.call(["sudo","systemctl", "start", "dhcpcd"])

  if os.path.isfile('/home/pi/current/app.py'):
    try:
      subprocess.call(["sudo","python3", "/home/pi/current/app.py"])
      logging(String = "Should be finished executing app.py")
    except: 
      logging(String = "ERROR: Something went wrong while starting app.py")
  else:
    logging(String = "No app.py")
