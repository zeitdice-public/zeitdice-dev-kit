#!/usr/bin/env python3
from glob import glob
import os
import sys
import datetime
from time import sleep
import serial
import pynmea2

portwrite = "/dev/ttyUSB2"
port = "/dev/ttyUSB1"
timeout = 300
latitude = 0.0
longitude = 0.0
last_lat = 1.0
last_lng = 2.0
last_loc = "%f, %f" % (last_lat, last_lng)

# Last Location in case GPS signal does not get us anything in a short time
try:
  if os.path.isfile('/home/pi/static/last_loc.txt'):
    with open('/home/pi/static/last_loc.txt', 'r') as f:
      last_loc =f.read()
    print("last_loc.txt (file existed): %s" % str(last_loc))
    last_lat = last_loc.split(',')[0]
    last_lng = last_loc.split(',')[1]
    print("last_loc.txt (lat): %s" % last_lat)
    print("last_loc.txt (lng): %s" % last_lng)
  else:
    with open('/home/pi/static/last_loc.txt', 'w') as f:
      f.write(last_loc)
    print("last_loc.txt (initiated file): %s" % str(last_loc))
except: 
  print("GPS failed to read / write file")
try:
  print("GPS: Connecting port")
  serw = serial.Serial(portwrite, baudrate = 115200, timeout = 1,rtscts=True, dsrdtr=True)
  serw.write('AT+QGPS=1\r'.encode())
  sleep(0.5)
  serw.write('AT+QGPSCFG="autogps",1\r'.encode())
  sleep(0.5)
  serw.write('AT+QGPS=0\r'.encode())
  sleep(0.5)
  serw.write('AT+QGPSDEL=1\r'.encode())
  sleep(0.5)
  serw.write('AT+QGPS=1\r'.encode())
  serw.close()
  sleep(0.5)

  print("GPS: Receiving GPS data")
  ser = serial.Serial(port, baudrate = 115200, timeout = 0.5,rtscts=True, dsrdtr=True)

  while timeout > 0:
    data = ser.readline().decode("utf-8")
    timeout -= 1
    try:
      msg = pynmea2.parse(data)
      if msg.latitude != 0.0:
        latitude = msg.latitude
        longitude = msg.longitude
        timeout = 0
        # Reset Last Location File with now newly found Location
        last_loc = "%f, %f" % (latitude, longitude)
        with open('/home/pi/static/last_loc.txt', 'w') as f:
          f.write(last_loc)
        print("Loc: %s / %s" %(latitude,longitude))
    except:
      # Nothing to talk about
      print("Noc: %s" %(data))
  ser.close()
  # If Lat is still nothing, we have to use what's in last_loc.txt
  if latitude == 0.0:
    latitude = last_lat
    longitude = last_lng
    print("Taking last_loc: %s / %s" %(latitude,longitude))
except:
  print("failed")
print("Final Loc: %s / %s" %(latitude,longitude))
