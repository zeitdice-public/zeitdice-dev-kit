#!/usr/bin/env python3

from glob import glob
import uuid
import os
import json
import sys
import requests
import time
from time import sleep
import datetime
from datetime import timedelta, date
import hashlib
import piexif
import subprocess
from PIL import Image, ImageChops
from fractions import Fraction


print("Try to manipulate EXIF of JPEG")
photo = "/Users/mschwanzer/Desktop/exif.jpg"

def to_deg(value, loc):
  if value < 0:
    loc_value = loc[0]
  elif value > 0:
    loc_value = loc[1]
  else:
    loc_value = ""
  abs_value = abs(value)
  deg =  int(abs_value)
  t1 = (abs_value-deg)*60
  min = int(t1)
  sec = round((t1 - min)* 60, 5)
  return (deg, min, sec, loc_value)

def change_to_rational(number):
  f = Fraction(str(number))
  return (f.numerator, f.denominator)

lat =43.6094067
lng =-79.49255795
altitude = 0

lat_deg = to_deg(lat, ["S", "N"])
lng_deg = to_deg(lng, ["W", "E"])

exiv_lat = (change_to_rational(lat_deg[0]), change_to_rational(lat_deg[1]), change_to_rational(lat_deg[2]))
exiv_lng = (change_to_rational(lng_deg[0]), change_to_rational(lng_deg[1]), change_to_rational(lng_deg[2]))

gps_ifd = {
    piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
    piexif.GPSIFD.GPSAltitudeRef: 1,
    piexif.GPSIFD.GPSAltitude: change_to_rational(round(altitude)),
    piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
    piexif.GPSIFD.GPSLatitude: exiv_lat,
    piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
    piexif.GPSIFD.GPSLongitude: exiv_lng,
}

gps_exif = {"GPS": gps_ifd}
exif_data = piexif.load(photo) # get original exif data first!
exif_data.update(gps_exif) # update original exif data to include GPS tag
exif_bytes = piexif.dump(exif_data)
piexif.insert(exif_bytes, photo)





img = Image.open(photo)
img_exif = img.getexif()



print(type(img_exif))
# <class 'PIL.Image.Exif'>

if img_exif is None:
    print("Sorry, image has no exif data.")
else:
    img_exif_dict = dict(img_exif)
    print(img_exif_dict)
    # { ... 42035: 'FUJIFILM', 42036: 'XF23mmF2 R WR', 42037: '75A14188' ... }
    for key, val in img_exif_dict.items():
        if key in ExifTags.TAGS:
            print(f"{ExifTags.TAGS[key]}:{repr(val)}")
