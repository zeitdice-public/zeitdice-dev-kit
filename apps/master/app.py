#!/usr/bin/env python3
# This script should take a photo and upload it.

from glob import glob
import uuid
import os
import json
import sys
import requests
import time
from time import sleep
import datetime
from datetime import timedelta, date
import hashlib
import subprocess

# Don't Change this unless debugging via SSH on an actual Dev Kit, if set to False and released as FW, cameras might not shutdown / wake up again
enable_reboot_shutdown = True

# For showing errors in exceptions during debugging on device: 
#except Exception as e: print(e)

try:
  # VERSION is replaced when utils/create_release is run and then set back again
  VERSION = 'APP_VERSION'

  # Try to get next wakeup datetime
  def get_wakeup_datetime():
    wakeup_datetime = "unknown"
    try:
      witty_schedule_log = subprocess.check_output(["sudo", "tail","-n20", "/home/pi/wittyPi/wittyPi.log"]).decode("utf-8")
      witty_schedule_log = witty_schedule_log.replace("\n", "")
      k = witty_schedule_log.rfind("Schedule next startup at: ")
      witty_schedule_log = witty_schedule_log[k+26:]
      witty_schedule_log = witty_schedule_log[:19] #2019-11-22 21:12:00
      wakeup_datetime = datetime.datetime.strptime(witty_schedule_log, '%Y-%m-%d %H:%M:%S')
    except:
      wakeup_datetime = "NA"
    return wakeup_datetime

  # Try to get uptime
  def get_uptime():
    uptime = "unknown"
    try:
      with open('/proc/uptime', 'r') as f:
        uptime = float(f.readline().split()[0])
        uptime = str(timedelta(seconds = uptime))
    except:
      uptime = "NA"
    return uptime

  def logging (msg):
    log = ('APP '+ VERSION + ' / ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' / Wake: ' + str(get_wakeup_datetime()) + ' / ' + msg + '\n')
    print(log)
    with open('/home/pi/static/log.txt','a') as file:
      file.write(log)

  def check_server():
    import urllib
    try:
      url = "https://api.timelapse.gallery"
      urllib.request.urlopen(url).close()
    except urllib.error.URLError:
      logging("Cannot connect to the server, sleep for 10")
      time.sleep(10)
      return False
    else:
      logging("Connected to the server")
      return True

  # Upload Files for Debugging
  def upload_file(file_name, file_location, device_uuid):
    try:
      payload = '{"filename": "%s"}' % file_name
      headers = {'Content-Type':'application/json'}
      host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/add_resource'
      # TODO: Verify= False since May 2019 because of certificate failure
      r = requests.post(host_url, data=payload, headers=headers, verify=False)
      status = r.status_code
      logging("Debug Upload: Sent Payload: %s" % str(file_name))
    except:
      logging("Debug Upload: ERROR: Could not send payload: %s" % str(file_name))
    if status == 201:
      logging("Debug Upload: Got 201 Response for: %s" % str(file_name))
      try:
        data = r.json()
        message = data['message']
        upload_url = data['data']['presigned_url_for_upload']
        logging("Server message: %s" % message)
        headers={'Content-Type': 'text/plain'}
        logging("Uploading File")
        # TODO: Verify= False since May 2019 because of certificate failure
        with open(file_location, "rb") as f:
          r = requests.put(upload_url, data=f, headers=headers, verify=False)
        if r.status_code == 200:
          logging("Debug Upload: Uploaded successful")
        else:
          logging("Debug Upload: Could not upload")
      except:
        logging("Debug Upload: ERROR: Could not send file")

  # Calculate the root-mean-square difference between two images
  def rmsdiff(im1, im2):
    h = ImageChops.difference(im1, im2).histogram()
    return math.sqrt(functools.reduce(operator.add,
      map(lambda h, i: h*(i**2), h, range(256))
    ) / (float(im1.size[0]) * im1.size[1]))

  from picamera import PiCamera, Color
  import psutil
  from shutil import copyfile
  import math
  import operator
  import functools
  import piexif
  from PIL import Image, ImageChops
  import serial
  import pynmea2
  from fractions import Fraction

  # Record start time
  logging("app.py started")

  PIL_VERSION = 0
  try:
    PIL_VERSION = Image.__version__
  except:
    logging("Cannot get Pillow")

  # Try to get uptime before anything else happens
  uptime_start = get_uptime()

  # Define UUID based on HW in case there is none yet
  if os.path.isfile('/home/pi/static/uuid.txt'):
    with open('/home/pi/static/uuid.txt', 'r') as f:
      device_uuid =f.read()
    logging("UUID (existed): %s" % str(device_uuid))
  else:
    device_uuid = hashlib.md5(uuid.getnode().encode('utf-8')).hexdigest()
    with open('/home/pi/static/uuid.txt', 'w') as f:
      f.write(device_uuid)
    logging("UUID (new): %s" % str(device_uuid))

  # Variables / Defaults
  status = 0 # HTTP Returns
  capture_full_res = 0
  diff_float = 0
  host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'
  headers = {'Content-Type':'application/json'}
  battery_level = '0' # We don't know any better on the Dev Kits
  resolution = '1920x1080'
  image_effect = 'denoise'
  rotation = '0'
  timezone = 'Etc/UTC'
  full_res_threshold = 10
  schedule = 'schedule.wpi'
  next_start = 'NA'
  status_only = 0
  portwrite = "/dev/ttyUSB2"
  port = "/dev/ttyUSB1"
  gps_timeout = 30
  latitude = 0.0
  longitude = 0.0
  altitude = 0
  last_lat = 1.0
  last_lng = 2.0
  last_loc = "%f, %f" % (last_lat, last_lng)
  gps_used_last_loc = 0

  # GPS
  try:
    logging("GPS: Connecting port")
    serw = serial.Serial(portwrite, baudrate = 115200, timeout = 1,rtscts=True, dsrdtr=True)
    serw.write('AT+QGPS=1\r'.encode())
    #serw.write('AT+QGPSCFG="autogps",1\r'.encode())
    #serw.write('AT+QGPSDEL=1\r'.encode())
    serw.close()
  except:
    logging("GPS Activation Failed")

  # Last Location in case GPS signal does not get us anything in a short time
  try:
    if os.path.isfile('/home/pi/static/last_loc.txt'):
      with open('/home/pi/static/last_loc.txt', 'r') as f:
        last_loc =f.read()
      logging("last_loc.txt (file existed): %s" % str(last_loc))
      last_lat = last_loc.split(',')[0]
      last_lng = last_loc.split(',')[1]
      logging("last_loc.txt (lat): %s" % last_lat)
      logging("last_loc.txt (lng): %s" % last_lng)
    else:
      with open('/home/pi/static/last_loc.txt', 'w') as f:
        f.write(last_loc)
      logging("last_loc.txt (initiated file): %s" % str(last_loc))
  except: 
    logging("GPS failed to read / write last_loc")

  # Check if year is < 2019 and reset time accordingly via NTP
  try:
    if (datetime.datetime.now().year < 2019):
      logging("ERROR: Year < 2019")
      subprocess.call(["sudo", "/etc/init.d/ntp", "stop"])
      subprocess.call(["sudo", "ntpd", "-q", "-g"])
      subprocess.call(["sudo", "/etc/init.d/ntp", "start"])
  except:
    logging("ERROR: Could not retrieve new datetime via NTP restart")

  # Try to get timezone
  try:
    with open('/etc/timezone', 'r') as f:
      timezone = f.readline().split()[0]
  except:
    logging("ERROR: Could not get timezone")

  server_attempts = 0
  while(server_attempts<6):
    server_connect = check_server()
    if (server_connect == True):
      break
    server_attempts = server_attempts + 1

  # Get Settings from settings.json
  try:
    with open('/home/pi/static/settings.json', "r") as json_file:
      data = json.load(json_file)
      logging(str(data['schedule']))
      logging(str(data['resolution']))
      logging(str(data['rotation']))
      logging(str(data['timezone']))
      logging(str(data['full_res_threshold']))
      logging(str(data['image_effect']))

      # Be super careful here about overwriting default settings
      if data:
        if data['resolution']:
          if data['resolution'] in ['3280x2464','1920x1080','1280x720','640x480','2592x1944']:
            resolution = data['resolution']
        if data['rotation']:
          if data['rotation'] in ['0','90','180','270']:
            rotation = int(data['rotation'])
        if data['gps_timeout']:
          if data['gps_timeout'] in ['0','30','120','180','270','390']:
            gps_timeout = int(data['gps_timeout'])
        if data['full_res_threshold']:
          if data['full_res_threshold'] in ['0', '10', '25', '50']:
            full_res_threshold = int(data['full_res_threshold'])
        if data['image_effect']:
          if data['image_effect'] in ['none', 'negative', 'solarize', 'sketch', 'denoise', 'emboss', 'oilpaint', 'hatch', 'gpen', 'pastel', 'watercolor', 'film', 'blur', 'saturation', 'colorswap', 'washedout', 'posterise', 'colorpoint', 'colorbalance', 'cartoon', 'deinterlace1', 'deinterlace2']:
            image_effect = data['image_effect']
        if data['schedule']:
          if data['schedule'] in ['schedule.wpi','10.wpi','20.wpi','60.wpi','10min6to6.wpi','10min6to8.wpi','10min8to8.wpi','20min6to6.wpi','10min6to10.wpi','20min6to10.wpi']:
            schedule = data['schedule']
        if data['status_only']:
          logging(str(data['status_only']))
          if data['status_only'] in ['0', '1']:
            status_only = int(data['status_only'])
  except:
    logging("Could not get settings from local file.")

  # Memory Level
  memory_size = str(psutil.disk_usage('/')[0]/(1024.0 ** 2))
  memory_free = str(psutil.disk_usage('/')[2]/(1024.0 ** 2))

  # Taking Photo
  file_name = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S') + '.jpg'
  logging("Taking Photo: %s" % str(file_name))

  # Get a small one first to compare
  try:
    with PiCamera() as camera:
      camera.resolution = (640, 480)
      camera.image_effect = image_effect
      camera.rotation = rotation
      time.sleep(2)
      camera.capture(file_name)
  except:
    logging("ERROR: Could not take / save SMALL photo")

  # Compare to last file and overwrite last file
  if os.path.isfile('/home/pi/static/last.jpg'):
    logging("There is a last.jpg already, let's compare")
    im1 = Image.open(file_name)
    im2 = Image.open("/home/pi/static/last.jpg")
    diff_float = rmsdiff(im1, im2)
    logging('Diff %s' % str(diff_float))
    # Done comparing, move the current image into last.jpg
    copyfile(file_name, '/home/pi/static/last.jpg')
    if diff_float > full_res_threshold:
      logging("More than %s changed" % full_res_threshold)
      # Take High Res Image for upload
      capture_full_res = 1
    else:
      logging("Less than %s changed" % full_res_threshold)
  else:
    # There is no last.jpg yet, (running the first time, so let's take a full res and copy current image (low res) into last.jpg)
    logging("last.jpg does not exist yet")
    capture_full_res = 1
    copyfile(file_name, '/home/pi/static/last.jpg')

  if capture_full_res == 1:
    try:
      with PiCamera() as camera:
        camera.resolution = resolution
        camera.image_effect = image_effect
        camera.rotation = rotation
        time.sleep(2)
        camera.capture(file_name)
    except:
      logging("ERROR: Could not take / save FULL RES photo")

  # GPS
  try:
    logging("GPS: Receiving GPS data")
    ser = serial.Serial(port, baudrate = 115200, timeout = 0.5,rtscts=True, dsrdtr=True)
    while gps_timeout > 0:
      gps_data = ser.readline().decode("utf-8")
      gps_timeout -= 1
      try:
        msg = pynmea2.parse(gps_data)
        if msg.latitude != 0.0:
          latitude = msg.latitude
          longitude = msg.longitude
          gps_timeout = 0
          # Reset Last Location File with now newly found Location
          last_loc = "%f, %f" % (latitude, longitude)
          with open('/home/pi/static/last_loc.txt', 'w') as f:
            f.write(last_loc)
          logging("Loc: %s / %s" %(latitude,longitude))
      except:
        pass #Nothing to talk about
        logging("Noc: %s" %(gps_data))
    ser.close()
    # If Lat is still nothing, we have to use what's in last_loc.txt
    if latitude == 0.0:
      latitude = last_lat
      longitude = last_lng
      gps_used_last_loc = 1
      logging("GPS: using last_loc: %s / %s" %(latitude,longitude))
    def to_deg(value, loc):
      if value < 0:
        loc_value = loc[0]
      elif value > 0:
        loc_value = loc[1]
      else:
        loc_value = ""
      abs_value = abs(value)
      deg =  int(abs_value)
      t1 = (abs_value-deg)*60
      min = int(t1)
      sec = round((t1 - min)* 60, 5)
      return (deg, min, sec, loc_value)

    def change_to_rational(number):
      f = Fraction(str(number))
      return (f.numerator, f.denominator)

    lat_deg = to_deg(float(latitude), ["S", "N"])
    lng_deg = to_deg(float(longitude), ["W", "E"])

    exiv_lat = (change_to_rational(lat_deg[0]), change_to_rational(lat_deg[1]), change_to_rational(lat_deg[2]))
    exiv_lng = (change_to_rational(lng_deg[0]), change_to_rational(lng_deg[1]), change_to_rational(lng_deg[2]))

    gps_ifd = {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSAltitudeRef: 1,
        piexif.GPSIFD.GPSAltitude: change_to_rational(round(altitude)),
        piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
        piexif.GPSIFD.GPSLatitude: exiv_lat,
        piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
        piexif.GPSIFD.GPSLongitude: exiv_lng,
    }

    gps_exif = {"GPS": gps_ifd}
    exif_data = piexif.load(file_name) # get original exif data first!
    exif_data.update(gps_exif) # update original exif data to include GPS tag
    exif_bytes = piexif.dump(exif_data)
    piexif.insert(exif_bytes, file_name)

  except:
    logging("GPS FAILED")
  logging("GPS: %s / %s" % (latitude,longitude))

  # Try to get uptime before upload
  uptime_before_upload = get_uptime()

  # Payload to be sent to API
  if status_only == 1:
    payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"%s","battery_level":"%s","sdSizeMB":"%s","sdFreeMB":"%s", "diff_float":"%s", "pil_version":"%s", "uptime_before_upload":"%s", "uptime_start":"%s", "capture_full_res":"%s", "latitude":"%s", "longitude":"%s", "gps_used_last_loc": "%s"}, "cameraSettings": {"resolution": "%s","rotation": "%s","gps_timeout": "%s","timezone":"%s","full_res_threshold":"%s","image_effect":"%s","schedule":"%s", "status_only":"%s"}, "next_wakeup_at": "%s"}' % (VERSION, battery_level, memory_size, memory_free, str(diff_float), PIL_VERSION, uptime_before_upload, uptime_start, capture_full_res, str(latitude), str(longitude), str(gps_used_last_loc), resolution, rotation, str(gps_timeout), timezone, full_res_threshold, image_effect, schedule, str(status_only), get_wakeup_datetime())
  else:
    payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"%s","battery_level":"%s","sdSizeMB":"%s","sdFreeMB":"%s", "diff_float":"%s", "pil_version":"%s", "uptime_before_upload":"%s", "uptime_start":"%s", "capture_full_res":"%s", "latitude":"%s", "longitude":"%s", "gps_used_last_loc":"%s"}, "cameraSettings": {"resolution": "%s","rotation": "%s","gps_timeout": "%s","timezone":"%s","full_res_threshold":"%s","image_effect":"%s","schedule":"%s", "status_only":"%s"}, "filename": "%s", "next_wakeup_at": "%s"}' % (VERSION, battery_level, memory_size, memory_free, str(diff_float), PIL_VERSION, uptime_before_upload, uptime_start, capture_full_res, str(latitude), str(longitude), str(gps_used_last_loc), resolution, rotation, str(gps_timeout), timezone, full_res_threshold, image_effect, schedule, str(status_only), file_name, get_wakeup_datetime())

  try:
    # TODO: Verify= False since May 2019 because of certificate failure
    r = requests.post(host_url, data=payload, headers=headers, verify=False)
    status = r.status_code
    logging("Sent initial Payload, response: %s" % str(status))
  except:
    logging("ERROR: Could not send initial payload")

  # check status code was successful
  if status == 201:
    logging("Got 201 Response")
    try:
      data = r.json()
      message = data['message']
      if status_only == 1:
        logging("Status Upload Only")
      else:
        upload_url = data['data']['presigned_url_for_upload']
        logging("Server message: %s" % message)
        headers={'Content-Type': 'image/jpeg'}
        logging("Uploading to presigned_url_for_upload: %s" % (file_name))
        # TODO: Verify= False since May 2019 because of certificate failure
        with open(file_name, "rb") as f:
          r = requests.put(upload_url, data=f, headers=headers, verify=False)
        if r.status_code == 200:
          logging("Uploaded File successful")
          os.remove(file_name)
        else:
          logging("ERROR for File Upload: %s " % file_name)
      try:
        if 'new_settings' in data:
          new_settings_data = data['new_settings']
          new_settings = json.dumps(new_settings_data)
          with open("/home/pi/static/settings.json", "w") as f:
            f.write(str(new_settings))
          if new_settings_data['schedule']:
            if new_settings_data['schedule'] != schedule:
              if new_settings_data['schedule'] in ['schedule.wpi','10.wpi','20.wpi','60.wpi','10min6to6.wpi','10min6to8.wpi','10min8to8.wpi','20min6to6.wpi','10min6to10.wpi','20min6to10.wpi']:
                new_schedule_file = "/home/pi/current/%s" % new_settings_data['schedule']
                subprocess.call(["sudo","cp", new_schedule_file, "/home/pi/wittyPi/schedule.wpi"])
                logging(String="Installed new %s" % new_settings_data['schedule'])
          if new_settings_data['timezone']:
            if new_settings_data['timezone'] != timezone:
              if new_settings_data['timezone'] in ['Etc/UTC','America/Toronto', 'America/Moncton', 'America/Vancouver', 'America/Edmonton', 'Europe/Vienna']:
                zoneinfo = "/usr/share/zoneinfo/%s" % new_settings_data['timezone']
                subprocess.call(["sudo","ln", "-fs", zoneinfo, "/etc/localtime"])
                subprocess.call(["sudo","dpkg-reconfigure", "--frontend", "noninteractive", "tzdata"])
                logging(String="Installed new timezone %s" % new_settings_data['timezone'])

          if new_settings_data['debug']:
            if new_settings_data['debug'] == "1":
              logging("Debug Log Request to upload debug data received")
              upload_file("log.txt", "/home/pi/static/log.txt", device_uuid)
              upload_file("wittyPi.log", "/home/pi/wittyPi.log", device_uuid)
              upload_file("schedule.log", "/home/pi/wittyPi/schedule.log", device_uuid)
              upload_file("wittyPi.log", "/home/pi/wittyPi/wittyPi.log", device_uuid)

          logging("Server had new_settings, let's reboot")
          # Run Witty Pi script to schedule and reboot
          subprocess.call(["sudo", "bash", "-c", "/home/pi/wittyPi/runScript.sh"])
          subprocess.call(["sudo", "reboot"])
      except:
        logging("Something went wrong trying to get / set new settings")
    except:
      logging("ERROR: Could not send image to presigned_url_for_upload")

  logging("-- Main Job done, getting ready to shutdown or reboot")

  try:
    wakeup_datetime = get_wakeup_datetime()
    if wakeup_datetime == "NA":
      logging("Cannot get wakeup time before shutdown (NA), let's try a reschedule")
      try:
        witty_schedule_response = subprocess.check_output(["sudo", "bash", "-c", "/home/pi/wittyPi/runScript.sh"]).decode("utf-8")
        witty_schedule_response = witty_schedule_response.replace("\n", "")
        logging("Needed WittyPi reschedule bc no known wakeup time: %s" % str(witty_schedule_response))
      except:
        logging("Cannot reschedule witty via bash, next was NA")
      time.sleep(10)
      if enable_reboot_shutdown:
        subprocess.call(["sudo", "reboot"])
    else:
      try:
        # wake up in the past or less than 100 seconds away, let's reschedule and reboot
        if wakeup_datetime < (datetime.datetime.now() + timedelta(seconds=100)):
          witty_schedule_response = subprocess.check_output(["sudo", "bash", "-c", "/home/pi/wittyPi/runScript.sh"]).decode("utf-8")
          witty_schedule_response = witty_schedule_response.replace("\n", "")
          logging("Needed WittyPi reschedule bc next startup was in the past (or less than 100 seconds away): %s" % str(witty_schedule_response))
          if enable_reboot_shutdown:
            subprocess.call(["sudo", "reboot"])
        else:
          if wakeup_datetime < (datetime.datetime.now() + timedelta(hours=6)):
            logging("Next startup time is in the future but not too far away, all good")
          else:
            witty_schedule_response = subprocess.check_output(["sudo", "bash", "-c", "/home/pi/wittyPi/runScript.sh"]).decode("utf-8")
            witty_schedule_response = witty_schedule_response.replace("\n", "")
            logging("Needed WittyPi reschedule bc next startup was too far in the future: %s" % str(witty_schedule_response))
            if enable_reboot_shutdown:
              subprocess.call(["sudo", "reboot"])
      except:
        logging("Cannot reschedule witty via bash and we don't know about timedelta, rebooting")
        if enable_reboot_shutdown:
          subprocess.call(["sudo", "reboot"])
  except:
    logging("Cannot get rebooting ideas before for shutdown, let's reboot")
    # At this point I need a reboot and hope for the best
    time.sleep(6)
    if enable_reboot_shutdown:
      subprocess.call(["sudo", "reboot"])

except:
  logging("-- Something went wrong in app.py")
  time.sleep(30)
  if enable_reboot_shutdown:
    subprocess.call(["sudo", "reboot"])

logging("-- Actually shutting down now")
if enable_reboot_shutdown:
  subprocess.call(["sudo", "shutdown", "-h","now" ])
