#!/usr/bin/env python3

# This script should take a photo and upload it.
from glob import glob
import uuid
import os
import json
import sys
import requests
import time
from time import sleep
import datetime
from datetime import timedelta
import hashlib
import subprocess

VERSION = "1.1.2"
battery_level = "100"
memory_size = "100"
memory_free = "100"
PIL_VERSION = "7"
uptime_before_upload  = "1"
uptime_start = "1"
file_name = "mode4.jpg"
resolution = "16MP"
device_uuid = "virtual"

# Let's get a photo from capture.c
subprocess.call(["sudo","./capture"])
new_file_name = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S') + '.jpg'
os.rename(file_name, new_file_name)
file_name = new_file_name
host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'

headers = {'Content-Type':'application/json'}

payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"%s","battery_level":"%s","sdSizeMB":"%s","sdFreeMB":"%s"}, "cameraSettings": {"resolution": "%s"}, "filename": "%s"}' % (VERSION, battery_level, memory_size, memory_free, resolution, file_name)

try:
  # TODO: Verify= False since May 2019 because of certificate failure
  r = requests.post(host_url, data=payload, headers=headers)
  status = r.status_code
  print("Sent Payload")
except:
  print("ERROR: Could not send payload")

# check status code was successful
if status == 201:
  print("Got 201 Response")
  try:
    data = r.json()
    message = data['message']
    upload_url = data['data']['presigned_url_for_upload']
    print("Server message: %s" % message)
    headers={'Content-Type': 'image/jpeg'}
    print("Uploading: %s" % (file_name))
    # TODO: Verify= False since May 2019 because of certificate failure
    with open(file_name, "rb") as f: 
      r = requests.put(upload_url, data=f, headers=headers)
    if r.status_code == 200:
      print("Uploaded successful")
      os.remove(file_name)
    else:
      print("Error: %s \n" % file_name)
      print("Could not upload")
  except:
    print("ERROR: Could not send image to presigned_url_for_upload")
