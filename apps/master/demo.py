#!/usr/bin/env python3

# This script should take a photo and upload it.
from glob import glob
import uuid
import os
import json
import sys
import requests
import time
from time import sleep
import datetime
from datetime import timedelta
import hashlib
import subprocess
import serial
import pynmea2

try:
  # VERSION is replaced when utils/create_release is run and then set back again 
  VERSION = 'DEMO_MODE'

  ################################################################################
  #                       HELPER FUNCTIONS ARE HERE                              #
  ################################################################################

  # Function for logging into log.txt and printing in the console
  # String takes a string to describe the log
  # use as logging(String = "__")
  def logging (String):
    log = ('DMO ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' / ' + ' / ' + String + '\n')
    print(log)
    with open('/home/pi/static/log.txt','a') as file:
      file.write(log)

  # Function to check server connection
  def check_server():
    import urllib
    try:
      url = "https://api.timelapse.gallery"
      urllib.request.urlopen(url).close()
    except urllib.error.URLError:
      logging(String = "Cannot connect to the server")
      time.sleep(10)
      return False
    else:
      logging(String = "Connected to the server")
      return True

  # Calculate the root-mean-square difference between two images
  def rmsdiff(im1, im2):
    h = ImageChops.difference(im1, im2).histogram()
    return math.sqrt(functools.reduce(operator.add,
      map(lambda h, i: h*(i**2), h, range(256))
    ) / (float(im1.size[0]) * im1.size[1]))

  ################################################################################
  #                             START OF SCRIPT                                  #
  ################################################################################

  from picamera import PiCamera, Color
  import psutil
  from shutil import copyfile
  import math
  import operator
  import functools
  from PIL import Image, ImageChops

  PIL_VERSION = 0
  try: 
    PIL_VERSION = Image.__version__
  except: 
    logging(String = "Cannot get Pillow")

  demo_runs = 100
  while(demo_runs>0):
    demo_runs = demo_runs - 1 
    # Try to get uptime
    try:
      with open('/proc/uptime', 'r') as f:
        uptime_start = float(f.readline().split()[0])
        uptime_start = str(timedelta(seconds = uptime_start))
    except:
      logging(String ="ERROR: Could not get uptime_start")

    # Wait a bit for wittyPi to be available 
    time.sleep(5)

    # Define UUID based on HW in case there is none yet
    if os.path.isfile('/home/pi/static/uuid.txt'):
      logging(String ="We got a uuid already in app.py")
      with open('/home/pi/static/uuid.txt', 'r') as f:
        device_uuid =f.read()
    else:
      device_uuid = hashlib.md5(uuid.getnode().encode('utf-8')).hexdigest()
      with open('/home/pi/static/uuid.txt', 'w') as f:
        f.write(device_uuid)
      logging(String ="New UUID was generated")

    # Variables / Defaults
    status = 0 # HTTP Returns
    capture_full_res = 0
    diff_float = 0
    uptime = 0
    host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'
    headers = {'Content-Type':'application/json'}
    battery_level = '0' # This will need to get replaced
    # Default Settings
    resolution = '1920x1080'
    image_effect = 'denoise'
    rotation = '0'
    timezone = 'Etc/UTC'
    full_res_threshold = 10
    schedule = 'schedule.wpi'
    gps = 0
    portwrite = "/dev/ttyUSB2"
    port = "/dev/ttyUSB1"
    latitude = 0
    longitude = 0
    altitude = 0
    gps_qual = 0

    # Check if year is < 2019 and reset time accordingly
    try: 
      if (datetime.datetime.now().year < 2019):
        logging(String ="ERROR: Year < 2019")
        subprocess.call(["sudo", "/etc/init.d/ntp", "stop"])
        subprocess.call(["sudo", "ntpd", "-q", "-g"])
        subprocess.call(["sudo", "/etc/init.d/ntp", "start"])
    except:
      logging(String ="ERROR: Could not retrieve new ntp")

    # Try to get timezone
    try:
      with open('/etc/timezone', 'r') as f:
        timezone = f.readline().split()[0]
    except:
      logging(String ="ERROR: Could not get timezone")

    # Record start time
    logging(String = "app.py started")

    server_attempts = 0
    while(server_attempts<6):
      server_connect = check_server()
      if (server_connect == True):
        break
      server_attempts = server_attempts + 1

    # Get Settings from settings.json
    try:
      with open('/home/pi/static/settings.json', "r") as json_file:
        data = json.load(json_file)
        print(data['schedule'])
        print(data['resolution'])
        print(data['rotation'])
        print(data['timezone'])
        print(data['full_res_threshold'])
        print(data['image_effect'])
        print(data['gps'])
        # Be super careful here about overwriting default settings
        if data:
          if data['resolution']: 
            if data['resolution'] in ['3280x2464','1920x1080','1280x720','640x480','2592x1944']:
              resolution = data['resolution']
          if data['rotation']:
            if data['rotation'] in ['0','90','180','270']:
              rotation = int(data['rotation']) 
          if data['full_res_threshold']:
            if data['full_res_threshold'] in ['0', '10', '25', '50']:
              full_res_threshold = int(data['full_res_threshold']) 
          if data['image_effect']:
            if data['image_effect'] in ['none', 'negative', 'solarize', 'sketch', 'denoise', 'emboss', 'oilpaint', 'hatch', 'gpen', 'pastel', 'watercolor', 'film', 'blur', 'saturation', 'colorswap', 'washedout', 'posterise', 'colorpoint', 'colorbalance', 'cartoon', 'deinterlace1', 'deinterlace2']:
              image_effect = data['image_effect']
          if data['schedule']:
            if data['schedule'] in ['schedule.wpi','10.wpi','20.wpi','60.wpi','10min6to6.wpi','10min8to8.wpi','20min6to6.wpi','10min6to10.wpi','20min6to10.wpi']:
              schedule = data['schedule']
          if data['gps']:
            if data['gps'] in ['0','1']:
              gps = data['gps']
    except:
      logging(String = "Could not get settings from local file.")

    # Memory Level
    memory_size = str(psutil.disk_usage('/')[0]/(1024.0 ** 2))
    memory_free = str(psutil.disk_usage('/')[2]/(1024.0 ** 2))
    #print ('Memory Size: ' + memory_size)
    #print ('Memory Free: ' + memory_free)

    # Take Photo
    logging(String ="Taking Photo")
    file_name = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S') + '.jpg'
    logging(String = file_name)

    # Get a small one first to compare
    try:
      with PiCamera() as camera:
        camera.resolution = (640, 480)
        camera.image_effect = image_effect
        camera.rotation = rotation
        time.sleep(2)
        camera.capture(file_name)
    except:
      logging(String ="ERROR: Could not take / save photo")


    # Compare to last file and overwrite last file
    if os.path.isfile('/home/pi/static/last.jpg'):
      logging(String = "There is a last.jpg already, let's compare")
      im1 = Image.open(file_name)
      im2 = Image.open("/home/pi/static/last.jpg")
      diff_float = rmsdiff(im1, im2)
      logging(String = 'Diff %s' % str(diff_float))
      # Done comparing, move the current image into last.jpg
      copyfile(file_name, '/home/pi/static/last.jpg')
      if diff_float > full_res_threshold:
        logging(String = "More than %s changed" % full_res_threshold)
        # Take High Res Image for upload
        capture_full_res = 1
      else:
        logging(String = "Less than %s changed" % full_res_threshold)
    else: 
      # There is no last.jpg yet, (running the first time, so let's take a full res and copy current image (low res) into last.jpg)
      logging(String = "last.jpg does not exist yet")
      capture_full_res = 1
      copyfile(file_name, '/home/pi/static/last.jpg')

    if capture_full_res == 1:
      try:
        with PiCamera() as camera:
          camera.resolution = resolution
          camera.image_effect = image_effect
          camera.rotation = rotation
          camera.sharpness = 50
          time.sleep(2)
          camera.capture(file_name)
      except:
        logging(String ="ERROR: Could not take / save full res photo")

    # Try to get uptime
    try:
      with open('/proc/uptime', 'r') as f:
        uptime_before_upload = float(f.readline().split()[0])
        uptime_before_upload = str(timedelta(seconds = uptime_before_upload))
    except:
      logging(String ="ERROR: Could not get uptime_before_upload")

    payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"%s","battery_level":"%s","sdSizeMB":"%s","sdFreeMB":"%s", "diff_float":"%s", "pil_version":"%s", "uptime_before_upload":"%s", "uptime_start":"%s", "capture_full_res":"%s", "latitude":"%s", "longitude":"%s", "altitude":"%s", "gps_qual":"%s"}, "cameraSettings": {"resolution": "%s","rotation": "%s","timezone":"%s","full_res_threshold":"%s","image_effect":"%s","schedule":"%s", "gps": "%s"}, "filename": "%s"}' % (VERSION, battery_level, memory_size, memory_free, str(diff_float), PIL_VERSION, uptime_before_upload, uptime_start, capture_full_res, latitude, longitude, altitude, gps_qual, resolution, rotation, timezone, full_res_threshold, image_effect, schedule, gps, file_name)

    try:
      # TODO: Verify= False since May 2019 because of certificate failure
      r = requests.post(host_url, data=payload, headers=headers, verify=False)
      status = r.status_code
      logging(String ="Sent Payload")
    except:
      logging(String ="ERROR: Could not send payload")

    # check status code was successful
    if status == 201:
      logging(String ="Got 201 Response")
      try:
        data = r.json()
        message = data['message']
        upload_url = data['data']['presigned_url_for_upload']
        print("Server message: %s" % message)
        headers={'Content-Type': 'image/jpeg'}
        print("Uploading: %s" % (file_name))
        # TODO: Verify= False since May 2019 because of certificate failure
        with open(file_name, "rb") as f: 
          r = requests.put(upload_url, data=f, headers=headers, verify=False)
        if r.status_code == 200:
          logging(String ="Uploaded successful")
          os.remove(file_name)
        else:
          print("Error: %s \n" % file_name)
          logging(String ="Could not upload")
      except:
        logging(String ="ERROR: Could not send image to presigned_url_for_upload")

    logging(String = "-- Starting all over again")
    time.sleep(10)
except:
  print("Something went wrong in demo.py")
  # At this point I need a reboot and hope for the best
  subprocess.call(["sudo", "reboot"])

