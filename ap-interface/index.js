var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var path = require('path');
const util = require('util');
var spawn = require('child_process').spawn;
var proc;
var exec = require('child_process').exec;

// const filePathWpaConf = './example.conf';
const filePathWpaConf = '/home/pi/current/config-mode-wpa-supplicant.conf';
const ssidRegex = /(ssid=\")(.*)(\")/g;
const pskRegex = /(psk=\")(.*)(\")/g;

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const executePromise = util.promisify(execute);


const streamPath = path.join(__dirname, 'stream');

app.use('/', express.static(streamPath));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/wifi_credentials', async (req, res) => {
  try {
    const fileContents = await readFile(filePathWpaConf, 'utf-8');

    const ssid = ssidRegex.exec(fileContents)[2];
    const psk = pskRegex.exec(fileContents)[2];

    // reset our regex objects (they are stateful!)
    ssidRegex.lastIndex = 0;
    pskRegex.lastIndex = 0;

    res.send({ ssid, psk })
  } catch(e) {
    res.status(500).send({ error: `Unkown error while fetching wifi credentials: ${e}` });
  }
});

app.post('/wifi_credentials', async (req, res) => {
  try {
    const { ssid, psk } = req.body;
    console.log(ssid, psk);

    const fileContents = await readFile(filePathWpaConf, 'utf-8');

    const newFileContents = fileContents
                .replace(ssidRegex, `$1${ssid}$3`)
                .replace(pskRegex, `$1${psk}$3`);

    console.log(newFileContents);

    await writeFile(filePathWpaConf, newFileContents, 'utf-8');

    await executePromise('sudo service networking restart');

    res.send({ ssid, psk })
  } catch(e) {
    res.status(500).send({ error: `Unkown error while setting wifi credentials: ${e}` });
  }
});

var sockets = {};

function execute(command, callback){
    exec(command, function(error, stdout, stderr){ callback(stdout); });
}

io.on('connection', function(socket) {
  sockets[socket.id] = socket;
  console.log("Total clients connected : ", Object.keys(sockets).length);

  socket.on('disconnect', function() {
    delete sockets[socket.id];

    // no more sockets, kill the stream
    if (Object.keys(sockets).length == 0) {
      app.set('watchingFile', false);
      if (proc) proc.kill();
      fs.unwatchFile('./stream/image_stream.jpg');
    }
  });

  socket.on('start-stream', function() {
    startStreaming(io);
  });

  socket.on('stop-stream', function() {
    stopStreaming(io);
  });
  socket.on('shutdown', function() {
    shutdown(io);
  });
  socket.on('reboot', function() {
    reboot(io);
  });
});

http.listen(80, function() {
  console.log('listening on *:80');
});

function shutdown() {
  console.log('Shutdown Requested');
  execute('shutdown -h now', function(callback){
    console.log(callback);
  });
}

function reboot() {
  console.log('Reboot Requested');
  execute('shutdown -r now', function(callback){
    console.log(callback);
  });
}

function stopStreaming() {
  console.log('stopStreaming Requested');
  if (Object.keys(sockets).length == 0) {
    app.set('watchingFile', false);
    if (proc) proc.kill();
    fs.unwatchFile('./stream/image_stream.jpg');
  }
}

function startStreaming(io) {
  console.log('startStreaming Requested');
  if (app.get('watchingFile')) {
    io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
    return;
  }

  const imagePath = path.join(streamPath, 'image_stream.jpg');

  var args = ["-bm", "-n","-w", "320", "-h", "240", "-o", imagePath, "-t", "999999999", "-tl", "800"];
  proc = spawn('raspistill', args);

  console.log('Command executed: ', `raspistill ${args.join(' ')}`);
  // console.log('Watching for changes...', proc);

  app.set('watchingFile', true);

  fs.watchFile(imagePath, function(current, previous) {
    console.log('file changed...', imagePath);
    io.sockets.emit('liveStream', 'image_stream.jpg?_t=' + (Math.random() * 100000));
  })

}
