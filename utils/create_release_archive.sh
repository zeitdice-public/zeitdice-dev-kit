#!/bin/bash

# sh ./utils/create_release_archive.sh 'COMMIT MESSAGE'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
APP="master"
cd "$DIR/../apps/"
APP_FOLDER="$DIR/../apps/$APP"
DATE_WITH_TIME=`date "+%Y%m%d-%H%M%S"`
APP_VERSION="$APP-$DATE_WITH_TIME"
APP_FILE="$APP_FOLDER/app.py"
sed -i -e 's/APP_VERSION/'$APP_VERSION'/g' $APP_FILE
rm "$APP_FILE-e"
RELEASE_FILE="$APP-$DATE_WITH_TIME.tar.gz"
CHECKSUM_FILE="$RELEASE_FILE.checksum"
RELEASE_MSG="$DATE_WITH_TIME Release with $1"

tar -zcv --exclude='.git' --exclude='.gitignore' --exclude='.DS_Store' -f $RELEASE_FILE -C $APP_FOLDER .

# AFTER Archive is made, we need to reset the APP_VERSION in app.py
sed -i -e 's/'$APP_VERSION/APP_VERSION'/g' $APP_FILE
rm "$APP_FILE-e"
CHECKSUM="$(shasum -a 256 $RELEASE_FILE | cut -f1 -d' ')"
echo $CHECKSUM

# Publish FW

PUBLISH_URL="https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/apps/$RELEASE_FILE"
echo $PUBLISH_URL
curl -X POST -H "Content-Type: application/json" \
    --data '{ "version":"'"$APP_VERSION"'", "changelog":"'"$RELEASE_MSG"'", "url":"'"$PUBLISH_URL"'", "checksum" : "'"$CHECKSUM"'"}' \
    https://www.zeitdice.com/api/v2/firmwares

# Do a git commit and push
git add $RELEASE_FILE -A
git commit -m "$RELEASE_MSG"
git push
