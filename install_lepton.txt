Thermal 2 Install, RPI 3A+

- Etcher Install Raspbian Buster
- Add wpa_supplicant to get on WIFI 
- touch ssh
- Install hardware (THIS IS BCM #, https://pinout.xyz/)
- SCL   BLUE  -> GPIO 3
- SDA   GREY  -> GPIO 2
- MISO  YELLOW -> GPIO 9
- CLK   PURPLE  -> GPIO 11
- CS    GREEN -> GPIO 8
- VSYNC ORANGE  -> GPIO 17

- 3V    RED
- GND   BLACK

Booting Up
At this point, I don't know why we would need to compile the kernel ourselves? If we need to we need to cross-compile since native on RPI would take days. Flir says its to enable DMA (Direct Memory Access)

ssh pi@192.168.2.44
sudo raspi-config
- Update

pi@raspberrypi:~ $ uname -r
4.19.75-v7+

sudo apt-get -y install git

wget https://novacoast-my.sharepoint.com/:u:/p/slewis/EV7LFwCXzWpMsHh_k7NTSXcBOZ8JRZdCSpI4sxeVQFD62Q?download=1
rename to flir.zip (mv)
unzip
cd ~/RaspberryPi/lepton_module

pi@raspberrypi:~/RaspberryPi/lepton_module $ make -C /lib/modules/`uname -r`/build M=$PWD modules
make: *** /lib/modules/4.19.75-v7+/build: No such file or directory.  Stop.

Based on that, I guess I have to compile? 
Trying Local compiling
sudo apt install git bc bison flex libssl-dev make
cd ~
git clone --depth=1 https://github.com/raspberrypi/linux
cd ~/linux
KERNEL=kernel7
make bcm2709_defconfig # This did not work until I installed bison flex libssl-dev make


make -j4 zImage modules dtbs
sudo make modules_install
sudo cp arch/arm/boot/dts/*.dtb /boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
sudo cp arch/arm/boot/zImage /boot/$KERNEL.img

after all this: reboot

pi@raspberrypi:~ $ uname -r
4.19.89-v7+



Jan 2nd, Trying again, 
- Fresh Buster
- Update raspi-config
- sudo apt-get update
- sudo apt-get dist-upgrade

sudo apt install git bc bison flex libssl-dev make
git clone --depth=1 https://github.com/raspberrypi/linux
reboot
cd ~/linux
Looking into this: https://www.thegeekstuff.com/2013/06/compile-linux-kernel/
with having make menuconfig 
needed sudo apt-get install libncurses-dev
tried to activate with Y what might be DMA? 

KERNEL=kernel7
make bcm2709_defconfig

make -j4 zImage modules dtbs
sudo make modules_install
sudo cp arch/arm/boot/dts/*.dtb /boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
# this might need $kernel replaced with kernel7.img, will need to check i fthe file exists? 
sudo cp arch/arm/boot/zImage /boot/$KERNEL.img

found a default config in the zip from sharepoint, might need to build with that? 

Looks like of_dma_configure no has 3 parameters and needs a bool at the end !!!
BOOM THAT WAS IT, added TRUE at end of of_dma_configure in RaspberryPi/lepton_module/flir_lepton.c after 
of_dma_configure(dev, of_node->parent, true);

make -C /lib/modules/`uname -r`/build M=$PWD modules
sudo make -C /lib/modules/`uname -r`/build M=$PWD modules_install

dtc flir-lepton-00A0.dts -o flir-lepton-00A0.dtbo
sudo cp flir-lepton-00A0.dtbo /boot/overlays/

sudo nano /boot/config.txt
#enable these
dtparam=i2c_arm=on
dtparam=i2s=on
dtparam=spi=on
#add this: dtoverlay=flir-lepton-00A0

also went into the raspi-config to enable i2c and spi just to be sure, seems like it did not work without that. 
sudo reboot

sudo depmod -a
sudo modprobe lepton

cd ~/RaspberryPi/lepton_control/

pi@raspberrypi:~/RaspberryPi/lepton_control $ make
cc -g -O2 -I../lepton_sdk -I.    -c -o bbg_i2c.o bbg_i2c.c
cc -g -O2 -I../lepton_sdk -I.    -c -o vsync_app.o vsync_app.c
cc -o vsync_app bbg_i2c.o vsync_app.o -L../lepton_sdk -llepton_sdk -ldl

pi@raspberrypi:~/RaspberryPi/lepton_control $ ./vsync_app
You are here.  masterDevice is: 2
bbg_init_i2c() called.
Failed I2C open(): No such file or directory
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(-1, 0x0002, 2): Bad file descriptor
LEP_GetOemGpioMode gpio_mode = 6 result = -26.
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(-1, 0x0002, 2): Bad file descriptor
LEP_SetOemGpioMode result = -26.
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(-1, 0x0002, 2): Bad file descriptor
LEP_GetOemGpioMode gpio_mode = 6 result = -26.


after reboot, better result: 
MAIN ISSUE NOW: 
pi@raspberrypi:~/RaspberryPi/lepton_control $ ./vsync_app
You are here.  masterDevice is: 2
bbg_init_i2c() called.
I2C device successfully opened.
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(3, 0x0002, 2): Remote I/O error
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(3, 0x0002, 2): Remote I/O error
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(3, 0x0002, 2): Remote I/O error
LEP_GetOemGpioMode gpio_mode = 6 result = -26.
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(3, 0x0002, 2): Remote I/O error
LEP_SetOemGpioMode result = -26.
bbg_read_byte_data_i2c(rx_adr=0x25e80, rx_data=0x25670, rx_size=2) called.
Writing big-endian address 0x0002
Failed I2C write(3, 0x0002, 2): Remote I/O error
LEP_GetOemGpioMode gpio_mode = 6 result = -26.


Taking something from the Pimoroni book: 
https://github.com/pimoroni/mlx90640-library
sudo apt-get install libi2c-dev


Craig helped and mentioned to check on i2cdetect -y 1 

there is nothing there and -l shows only the one that any RPI has, so we have an issue here, might be wiring, power should be good, had a 3.3 reading on the breakout board but that does not mean the Amp is enough. 

TODO: Get new jumpers at office and try with a different pi? 

pi@raspberrypi:~/RaspberryPi/lepton_control $ sudo i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
pi@raspberrypi:~/RaspberryPi/lepton_control $ sudo i2cdetect -l
i2c-1 i2c         bcm2835 I2C adapter               I2C adapter

Tried on battery pack, assuming enough AMPS, same result 
