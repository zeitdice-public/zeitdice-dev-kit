#!/bin/bash

OLD_DIR=/home/pi/old
CURRENT_DIR=/home/pi/current
PROPOSED_DIR=/home/pi/proposed

# if ![ -d "$CURRENT_DIR" ]; then
    # no current directory...should never happen
    # check for old code and link back

#    if [ -d "$OLD_DIR" ]; then
#        mv $OLD_DIR $CURRENT_DIR
#    fi

#    sudo reboot
#fi;

#if [ -d "$PROPOSED_DIR" ]; then
    # proposed unpacked firmware...
    # if it is OK, make it the new code

    # for now...we do not have checks (checksum etc.)
#    mv $CURRENT_DIR $OLD_DIR
#    mv $PROPOSED_DIR $CURRENT_DIR

#    sh $CURRENT_DIR/install.sh

#    sudo reboot
#fi;

#if [ -d "$OLD_DIR" ]; then
    # remove old version
#    rm -rf $OLD_DIR
#fi;


if [ -d "$PROPOSED_DIR" ]; then
  cp -R $PROPOSED_DIR/* $CURRENT_DIR
  
  cd $CURRENT_DIR
  if [ -f $CURRENT_DIR/install-app.sh ]; then
    . $CURRENT_DIR/install-app.sh;
  fi;
  
  rm -rf $PROPOSED_DIR
  
  reboot
fi;

