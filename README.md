# ZEITDice Dev Kit

**This is work in progress as of Feb 3rd, 2020.**

ZEITDice Dev Kit is an autonomous timelapse camera (using solar panels and cellular data connectivity) capturing photos on a schedule and upload them to the cloud with a Raspberry PI. All of this was developed to interact with [ZEITCloud] (https://www.zeitdice.com) but can be easily adapted for your own needs.

This documentation was generated during our internal processes and is not meant for beginners to start with. If you are an advanced user of unix systems and have some electronics experience you might be able to create a timelapse camera without having to do everything from scratch, however, we recommend you look into the solutions zeitdice.com offers if you need a reliable timelapse solutions quickly deployed. 

## Hardware (for assembly notes see bottom of this file)

* [Raspberry Pi 3 Model A+] (https://elmwoodelectronics.ca/collections/raspberry-pi/products/raspberry-pi-3-model-a)
* [Raspberry Pi Camera V2 8MP] (https://elmwoodelectronics.ca/products/raspberry-pi-camera) or * [Arducam 16MP] (https://www.arducam.com/product/16mp-imx298-color-mipi-camera-module-raspberry-pi/)
* SD Card 16 or 32GB, choose the fastest for best boot up times
* Enclosure Polycase [ml-45f-1508] (https://www.polycase.com/ml-45f-1508), polycarbonate, clear lid (we now cut a hole in it and hot glue a lens in it, like this one: Platinum Series 52mm Camera Polarizing Filter (PT-MCCP52-C))
* Laser Cut Mounting Plate (see folder /mechanical), spray some WD40 into the 4 screw holes to ensure smooth disassembly in the future (this is only mentioned because of issues in the past). Use black acrylic, everything else might reflect and leak into the images.
* [WittyPi Mini] (https://elmwoodelectronics.ca/products/witty-pi-mini-realtime-clock-rtc-and-power-management-for-raspberry-pi) [PDF Manual] (http://www.uugear.com/doc/WittyPiMini_UserManual.pdf)
* Sixfab LTE Shield + Quectel EC25 + Pulse Antenna + SIM Card
* Voltaic Battery
* Toggle Switch for Mode (Capture, OFF, Config)
* Silica bag to be glued somewhere inside to collect moisture
* [M3 male to female hex standoffs to mount everything] (https://www.amazon.ca/gp/product/B071RXSMW6/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1) 
* 2x Machine Screws 10-32 x 1/2 inch
* Aluminum Flat Bar 0.5 x 1 x 3.5 inch to be drilled and threaded for mounting 


## Prep: SD Card Setup
* Format SD Card as FAT32 with name ZEITDICE [use the official formatting tool from the SD Association] (https://www.sdcard.org/downloads/formatter_4/)
* Flash SD Card with [Raspbian Buster Lite] (https://downloads.raspberrypi.org/), on Mac you can use [Etcher] (https://www.balena.io/etcher/)
* Mount SD Card (on Mac or PC, not RPI)
* Enable ssh by running “touch ssh” after “cd /Volumes/boot” on Mac 
* Enable WIFI connection by creating a file called “wpa_supplicant.conf” in /boot that looks like this: 

	```
	ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
	update_config=1
	country=CA
	
	network={
	  ssid="WIFINAME"
	  psk="WIFIPASSWORD"
	  key_mgmt=WPA-PSK
	}
	```

* Add the following at the bottom of /boot/config.txt (should we be able to do this via install.sh? Tried, did not work)

  ```
	max_usb_current=1
	disable_splash=1
	dtoverlay=pi3-disable-bt
	start_x=1
	gpu_mem=128
	disable_camera_led=1
	```


* Unmount SD Card

## Hardware and Software Installation
* Make sure Raspberry PI is unplugged
* Connect Raspberry PI Camera 
* Plug in SD Card
* Make sure NO other hardware is connected yet (WittyPi, LTE Shield)
* Plug Raspberry PI into power
* RPI should boot (can watch with screen connected via HDMI) and connect to WIFI given in wpa_supplicant.conf
* SSH into the RPI with ssh pi@IPADDRESSPROVIDEDBYWIFIAP
* Run “sudo raspi-config”
	* Change User Password: timeispower
	* Boot Options: Disable Wait for Network on boot
	* Set Hostname to zeitdice-dev-kit
	* Boot Options: Choose Console (B1, Not Autologin)
	* Advanced Options: Expand File System
	* Finish 
* Choose Reboot Now in raspi config interface or reboot “sudo reboot”
* SSH into the RPI again, now with new password

	``` 
	wget https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/install_16MP.sh
	sudo bash install_16MP.sh
	```


* Camera install 
https://github.com/ArduCAM/MIPI_Camera/tree/master/RPI
	
* Install Witty PI

	```
	sudo sh installWittyPi.sh
	```

* Add Cron Job to start on reboot (Ensure there is a blank space at the end of the crontab file):
	
	```
	crontab -e
	``` 
	add: 
	
	```
  @reboot sudo sh /home/pi/updater/updater-main.sh
  @reboot sudo python3 /home/pi/current/operator.py
	```

* After install is done, shut down and unplug RPI

	```
	sudo shutdown now -h
	```
* Mount LTE Shield with Quectel EC25, Antenna and SIM card, connect USB of LTE shield to RPI USB, then Witty Pi, both switches on. 
* Plug the power source USB now **into the Witty PI**, not the RPI directly. Push little button on Witty Pi if it does not start automatically 
* Boot up again and SSH in (this time connect via Access Point first, the RPI will be in Setup mode broadcasting a network TimelapseCameraLTE with password timeispower IP will be 192.168.4.1)
* Install LTE Shield (need to know the APN for the SIM card, for example 'm2minternet.apn' for Rogers in Canada, T-Mobile with internet.gma.iot)

	```
	sudo sh install-lte.sh
	```
* Check if LTE is working, Need to set routing if using LTE: 
	
	```
	sudo pon
	sudo route del default
	sudo route add default ppp0
	```
* Try LTE by running sudo pon, then ping zeitdice.com or 1.1.1.1 if DNS does not work yet

* check if the default app was installed and has now in /home/pi/proposed/schedule.wpi the right schedule file (will be installed next reboot)
* check if app.py is in /home/pi/current
* cd into /home/pi/wittyPi and run sudo ./wittyPi.sh to check if time is correct and wakeup has been scheduled, if not, run sudo ./runScript.sh and confirm in wittyPi interface that it is in the future. 
* Reboot and SSH back into the RPI

* Remove app.py if you are debugging so after entering Capture Mode app.py won't get executed (which would probably take an image and go back to sleep)
* If you run sudo python3 operator.py now it will go into Config Mode (closes this connection and goes into Config Mode. Look for a new WIFI AP called TimelapseCameraLTE, password is **timeispower** 

* Add Switch between GPIO 13 (Board, not BCM layout) and 3.3V PIN to switch between modes, if switch is closed, PIN 13 goes high and on reboot Capture Mode is entered (which will execute app.py and go back to sleep). 

From here on the following will happen:

## Flow
* Cron job starts updater/updater-main.sh on each reboot
* Cron job starts operator.py on each reboot
	* Config or Capture Mode is started
	* In Capture Mode app.py is executed which has to make sure a wake up time is set before the device shuts down again and shut down (do whatever that app does in between)

## Operation Modes

### Config Mode (default when GPIOs untouched)
* RPI starts WIFI Access Point based on config-mode-wpa-supplicant.conf and can be connected to
* RPI will have IP address 192.168.4.1 and hand out via DHCP IP addresses between 192.168.4.2 and 192.168.4.20 with a 24h lease time (this can be configured in /etc/dnsmasq.txt)
* node.js App is started on port 80 (needs to run as root to run on 80) and shows viewfinder and controls

### Capture Mode (entered by operator.py when GPIO 13 is high 3.3V)
* starts sudo python3 /home/pi/current/app.py

## Apps
In folder apps there are folders each with an install-app.sh that will install whatever app needs to run on the HW, initially this is the 'master' app and the script will be a template for more apps. The install-app.sh will replace the app.py file that the operator.py calls but it can do other things as well, like install more libs etc. 


## TODO

* Reduce bootup time to minimum
* Need a way in ap-interface to change WIFI credentials for capture-mode-wpa-supplicant.conf (and have a radio button to decide if LTE or WIFI should be used.
* To make SD card setup faster an image with just 2GB should be made to flash onto SD cards (complete installation done) and then the autoexpand of the raspi used.

### Done
* Updated to Polycase, clearer view and better mounting option (more stable by threading aluminum directly outside, better leakage protection as well less holes in enclosure)
* Ensure time is synced, witty only does it a minute after startup (according to their docs) and thats too late and time drifts, 30 min so far in 2 weeks, according to their docs as well we should not use ntp , can't do through wittyPi/syncTime.sh but maybe can be done through https://github.com/uugear/Witty-Pi-2/tree/master/wittyPi
* Need a way to get battery level (can't do)
* When the install-app script from default sample app in install.sh creates /wittyPi folder and moves schedule.wpi there, the actual wittyPi install script fails. 
* operator.py should get new apps and install them if needed (rudimentary there, but does not work)
* Need a rescuebot script that checks if uptime has been longer than expected and reboot device (send a debug file to API)
* ap-interface viewfinder stream does not work
* While operator.py seems to get reliably started, it does not seem like it always gets to executing app.py, then is just stuck on.
* Need a way in ap-interface to change WIFI credentials for Config Mode Access Point in config-mode-wpa-supplicant.conf
* operator.py should always call the API to send a device update and get updates if needed
* UUID determined by python seems to switch some times? Although it should not, maybe we create a UUID.txt file on first call of operator.py that holds it and if that file exists we use that UUID rather than asking the HW to provide one on each call.
* Added uptime to operator.py logging and OP / APP to know who wrote the log line
* Need a script that runs before every shutdown to ensure a wakeup is set and the time is in the future and at not more than 3 hours away. (we won't know when shutdown happens when power is out for example) 
* added PIL and ffmpeg to install script
* added reset of time to UTC to install script


## Notes and FAQ

* Script Gen for Schedules: http://www.uugear.com/app/wittypi-scriptgen/
* When in Capture Mode the device might only be on for a few seconds before going to sleep again, that's why there is a HW switch to tell operator.py to go into Config Mode.
* If making changes to the Access Point Interface (ap-interface), which is node.js based, the directory ap-interface has to be packed as a tar file to replace the existing one in this directory, since that tar file is what install.sh downloads.
	```
	tar -czf ap-interface.tar ap-interface
	```
* After install, WittyPi might still have some schedule running that will turn it off and on.
* All the .txt files here are being changed to .conf after install.sh runs on the RPI
* To check on boot up time for further optimization, systemd-analyze blame is a good tool
* If Pi never goes to sleep, you might have plugged in the power supply into the RPI instead of the Witty
* Make sure system is set to run in ETC/UTC (this is done in install.sh since April 26th 2019)
	```
	sudo ln -fs /usr/share/zoneinfo/UTC /etc/localtime
	sudo dpkg-reconfigure --frontend noninteractive tzdata
	```
* To create and upload new FW Version
  ``` 
  sh ./utils/create_release_archive.sh 'RELEASE / GIT MESSAGE'
  ```
* Force NTP Update 
	``` 
		sudo /etc/init.d/ntp stop
		sudo ntpd -q -g
		sudo /etc/init.d/ntp start
	```
## Hardware Assembly Notes

Go in order, otherwise you have to unscrew the mounts. This assumes SD card is ready to go and was tested in other device.

* Connect Camera to RPI A 
* Screw standoffs into RPI (11mm long), clip off ends on the bottom side, not too short, just 1 mm
* Plugin USB cable for LTE shield and lead it through the inside of the standoff (above the RPI) SIM Card wont go in unless Micro USB on LTE shield is unplugged
* Mount Sixfab LTE Shield
* Put Quectel EC-25 (E or A depending on location) into LTE Shield
* Connect Antenna Pulse (3 connectors) to Quectel EC-25, each connector has it's place (LTE Diversity -> DIV, GPS -> GNSS, LTE Primary -> MAIN) 
* Tape electric tape over all of the EC-25 and the antenna connector to prevent shorts from the Witty
* Screw standoffs (5mm long, that's right, you need to clip some 6mm down to 5mm otherwise there will be connection issues) onto the GPIO side of the LTE Shield
* Mount Witty Pi Mini
* Turn both switches on Witty Pi on
* Screw standoffs (6mm long) onto the GPIO side of the Witty 
* Screw standoffs (12mm long) onto the other side of the LTE Shield
* Electrical Tape all around the top / bottom enclosure, just to be sure, covering the gap
* Silicone / Hot glue inside around the switch and cable gland just to be sure

## Enclosure (Polycase) Prep, with drill press
* Use 3/8 inch drill on back of case to remove material where 2 screw heads will go to mount aluminum mounting bar at 
* Use 1/2 inch drill to make hole in bottom of case for cable gland, don't get too close to edge of case since inside still needs to fit the nut of the gland
* Use 1/8 inch drill to enlarge mounting base holes on the left and right side (4 in total) for later hot glue inserting of standoffs, 2 of the longest ones, set depth on drill press to not drill through all the way but enough to hit the base bottom so the standoffs go in all the way. 
* Use 1/4 inch drill to drill hole for switch, be aware that room inside is tight, need to fit battery and lid and PCB behind switch
* Use 1/4 drill to remove mounting posts in bottom of the case (2) so battery will fit into the case nicely


## Aluminum Mounting Bar
* For the 1/4 20 NC threading hole, use drill 15/64, dead centre of the alu bar bottom
* For the enclosure mounts (10 32NC), use 11/64 drill 

### Toggle Switch PCB Connections
* Toggle Switch needs to be mounted onto the logo side of the PCB and soldered on the back side. 

Other wires to be soldered on with the following numbers: 

![Toggle Switch PCB Description](https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/mechanical/ToggleSwitchPCB_Desc.png)


7 - 10 should be ignored and the mentioned wires connected directly instead. 
1. +5V wire coming from USB Male plugged into Voltaic Battery (PIN 1 on connector)
2. GND wire coming from USB Male plugged into Voltaic Battery (PIN 4 on connector)
3. GND wire going to Witty Pi GND soldered next to its Micro USB Connector
4. +5V wire going to Witty Pi 5V soldered next to its Micro USB Connector
5. Solder wire from Witty Pi GPIO 1 (3V3) here (there is a small pad next to where the pins would come through)
6. Solder wire from Witty Pi GPIO 13 here (this is between the UUGear Logo and the GP17 writing, where there is no print)
IGNORE:
7. Solder the external GND wire on here (coming from a Voltaic solar connector that went through the installed cable gland already! Don't miss this otherwise you won't get the cable through the cable gland / enclosure)
8. Solder the external +V here, same as in 7, it had to come through the cable gland in the enclosure by now. 
9. After stripping the USB Male off the cable that came with the 15V Voltaic battery, solder the +V wire onto here.
10. From the same cable as used in 9, solder GND here.


### Disclaimer

We don't provide any support for this material and code or warrant that it works or are responsible for any harm that this can do to your or your hardware. Use at your own risk. You can buy a Dev Kit ready to deploy at [zeitdice.com] (https://www.zeitdice.com).

![ZEITDICE logo](https://d33onyjek027b4.cloudfront.net/static_files/zeitdice_logo_transparent_black.png)
