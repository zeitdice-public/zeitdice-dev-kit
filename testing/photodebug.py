#!/usr/bin/env python3

# This script should take a photo and upload it.
VERSION = 'PHOTODEBUG'

################################################################################
#                       HELPER FUNCTIONS ARE HERE                              #
################################################################################

# Function for logging into log.txt and printing in the console
# String takes a string to describe the log
# use as logging(String = "__")
def logging (String):
    # logs data in the format TIMESTAMP / WAKEUPALARM / BATTERYLVL / String \n
    log = ('APP ' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' / ' + ' / ' + String + '\n')
    print(log)
    with open('/home/pi/static/log.txt','a') as file:
        file.write(log)
        file.close()


# Function to check server connection
# I am using a ping test
def check_server():
    import urllib
    try:
        url = "https://api.timelapse.gallery"  # Need to put our API
        urllib.request.urlopen(url).close()
    except urllib.error.URLError:
        logging(String = "Cannot connect to the server")
        time.sleep(10)
        return False
    else:
        logging(String = "Connected to the server")
        return True


################################################################################
#                             START OF SCRIPT                                  #
################################################################################

# This script needs to be the most robust it can be, it will be the only thin keeping the camera connected!

from glob import glob
import uuid
import os
import sys
import requests
import time
import datetime
import hashlib
import subprocess
import picamera
import psutil

time.sleep(3) # Not sure if we need this


# Record start time
logging(String = "photodebug.py started")


################################################################################
#                      CHECK FOR SERVER CONNECTIVITY                           #
################################################################################
server_attempts = 0
while(server_attempts<6):
    server_connect = check_server()
    if (server_connect == True):
        break
    server_attempts = server_attempts + 1
# Exits if there is no wifi
  #if (server_connect == False):  # Don't know what to do here yet
      #sys.exit()


# Define UUID based on HW in case there is none yet
if os.path.isfile('/home/pi/static/uuid.txt'):
  logging(String ="We got a uuid already in app.py")
  f=open("/home/pi/static/uuid.txt", "r")
  if f.mode == 'r':
    device_uuid =f.read()
else: 
  device_uuid = str(uuid.getnode())
  device_uuid = hashlib.md5(device_uuid.encode('utf-8')).hexdigest()
  f= open("/home/pi/static/uuid.txt","w+")
  f.write(device_uuid)
  f.close()
  logging(String ="New UUID was generated")


# Battery Level
battery_level = '100' #str(pj.status.GetChargeLevel().get('data'))
print ('Battery Level: '+ battery_level)

# Memory Level
memory_level = str(psutil.disk_usage('/'))
print ('Memory Level: ' + memory_level)

status = 0
host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'
headers = {'Content-Type':'application/json'}


# Take Photo
logging(String ="Taking Photo")
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
print (st)
file_name = st + '.jpg'
headers = {'Content-Type':'application/json'}
print (file_name)

try:
  with picamera.PiCamera() as camera:
    camera.resolution = (3280, 2464)
    camera.capture(file_name)
    f = open(file_name, 'rb')
    statinfo = os.stat(file_name)
except:
    logging(String ="ERROR: Could not take / save photo")


# form the http request - see notes above
payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"%s","battery_level":"%s","sdSizeMB":"32000","sdFreeMB":"%s"}, "filename": "%s"}' % (VERSION, battery_level, memory_level, file_name)

try:
    r = requests.post(host_url, data=payload, headers=headers)
    status = r.status_code
    logging(String ="Sent Payload")
except:
    logging(String ="ERROR: Could not send payload")

# check status code was successful
if status == 201:
  logging(String ="Got 201 Response")
  data = r.json()
  message = data['message']
  upload_url = data['data']['presigned_url_for_upload']
  print("Server message: %s" % message)
  headers={'Content-Type': 'image/jpeg'}
  print("Uploading: %s" % (file_name))
  start = time.time()
  r = requests.put(upload_url, data=f, headers=headers)
  if r.status_code == 200:
    finish = time.time()
    upload_time = finish-start
    file_size = statinfo.st_size/1024
    print("Success: %s in %s secs (%s KBps)\n" % (file_name,upload_time,(file_size/upload_time)))
    logging(String ="Uploaded successful")
    os.remove(file_name)
  else:
    print("Error: %s \n" % file_name)
    logging(String ="Could not upload")


################################################################################
#                            SETTING UP ALARM                                  #
################################################################################


logging(String = "-- Done")
