import cv2
import numpy as np
import json
import time
# For capturePic()
from picamera import PiCamera
from picamera import array
from time import sleep
import io
import os
import sys
import requests
import datetime
import hashlib
import subprocess

# For retriving exposure_times
from PIL import Image
from PIL.ExifTags import TAGS

def get_field (exif,field) :
  for (k,v) in exif.items():
     if TAGS.get(k) == field:
        return v

# Obtain exposure from JSON file
def obtainEV():
    #with open('/home/pi/zeitcell/test_settings.json') as json_file:
    #    data = json.load(json_file)
    #    exposure_values = data['HDR']['EV']
    exposure_values = [-2,0,2]
    number_of_exposure_values = len(exposure_values)

    return exposure_values, number_of_exposure_values

# Take pictures using picamera
def capturePic():


    camera = PiCamera()
    camera.resolution = (720, 480)
    #camera.resolution = (3280, 2464)
    camera.flash_mode = 'auto'
    # To capture image as a file-object
    my_stream = io.BytesIO()

    #To avoind io
    #my_stream = array.PiRGBArray(camera)

    images = []
    # Exposure times
    times = []

    #camera.exposure_mode = 'off'  #Let automatically set the mode
    #camera.iso = 800
    #camera.framerate = 1/9
    #camera.shutter_speed = 2000
    #camera.start_preview()
    # For focusing
    sleep(2)

    i = 0
    for i in range(number_of_exposure_values):
        # exposure_compensation ranges from -25 to +25 representing EV of
        # -4 to +4 in 6 steps per EV
        camera.exposure_compensation = exposure_values[i]#*6
        print("EV Value:", camera.exposure_compensation)
        sleep(2)
        #camera.capture(my_stream, format = 'jpeg')
        #with array.PiRGBArray(camera) as my_stream:
        #camera.capture(my_stream, format = 'bgr')
        #print("my_stream:", my_stream)
        # Testing if EV is working
        camera.capture("image%d.jpg" %i)

        # Retrieving exposure_speed. When shutter_speed = 0 (automatic mode)
        # the exposure_speed returns actual shutter speed of the aperture.
        es = camera.exposure_speed
        #Since exposure speed returns values in micro secs
        es = es/1000000.00
        #print(camera.exposure_speed)
        print("Exposure speed:", es)
        #print("Shutter speed:", camera.shutter_speed/1000000.00)

        # Construct numpy array from the stream
        #data = np.fromstring(my_stream.getvalue(), dtype=np.uint8)
        #print("my_stream converted to numpy array as data", data)

        # Decode the image from the array, preserving color
        #image = cv2.imdecode(data, 1)
        # OpenCV returns an array with data in BGR order. If you want RGB inste$
        # use the following...
        #image = image[:, :, ::-1]

        # To avoid encoding and decoing and to speed up the process
        #image = my_stream.array

        #images.append(image)

        im = cv2.imread("image%d.jpg" %i)
        images.append(im)


        # Picamera capturing to a PIL image
        #img_PIL = Image.open(my_stream)
        img_PIL = Image.open("image%d.jpg" %i)

        # For exposure_time
        exif = img_PIL._getexif()
        time = get_field(exif,'ExposureTime')
        time = time[0]/time[1]

        times.append(time)

        #sleep(2)
        # Clear stream between captures
        #my_stream.truncate()
        # "Rewind" the stream to the beginning
        #my_stream.seek(0)

    camera.stop_preview()

    return images, np.asarray(times, dtype=np.float32)

# Read images
'''def readImagesAndTimes():

    times = np.array([ 1/30.0, 0.25, 2.5, 15.0 ], dtype=np.float32)
    filenames = ["img_0.033.jpg", "img_0.25.jpg", "img_2.5.jpg", "img_15.jpg"]
    images = []
    for fn in filenames:
        im = cv2.imread(fn)
        images.append(im)

    return images, times'''


# Running as a script
if __name__ == '__main__':
    # Read images and exposure times
    #images, times = readImagesAndTimes()
    exposure_values, number_of_exposure_values = obtainEV()
    print("Parsed exposure values")
    print(exposure_values)
    print(number_of_exposure_values)
    print("Inside capturePic() function")
    images, times = capturePic()
    print("Final list of images and times")
    #print(images)
    print(times)
    print(type(times))
    # Define UUID based on HW in case there is none yet
    if os.path.isfile('/home/pi/static/uuid.txt'):
       f=open("/home/pi/static/uuid.txt", "r")
       if f.mode == 'r':
          device_uuid =f.read()
       f.close()
    else:
        device_uuid = str(uuid.getnode())
        device_uuid = hashlib.md5(device_uuid.encode('utf-8')).hexdigest()
        f= open("/home/pi/static/uuid.txt","w+")
        f.write(device_uuid)
        f.close()

    host_url = 'https://api.timelapse.gallery/api/v2/sources/'+device_uuid+'/statuses'
    headers = {'Content-Type':'application/json'}
    # Align images
    # MTB is invariant to exposure time, so can be aligned w/o need for exposure times
    alignMTB = cv2.createAlignMTB()
    alignMTB.process(images, images)  # .process(input array, output array)

    # Obtain Camera Response Function (CRF)
    calibrateDebevec = cv2.createCalibrateDebevec()
    responseDebevec = calibrateDebevec.process(images, times)

    # Merge images into an HDR linear image
    mergeDebevec = cv2.createMergeDebevec()
    hdrDebevec = mergeDebevec.process(images, times, responseDebevec)
    # Save HDR image.
    cv2.imwrite("hdrDebevec.hdr", hdrDebevec)
    print("saved hdrDebevec.hdr ")


    # Tonemap using Drago's method to obtain 24-bit color image
    # The parameters were obtained by trial and error.
    # .createTonemapDrago(gamma, saturation, bias)
    # paramater 'bias' equal to 0.85 by default and ranges [0,1]
    tonemapDrago = cv2.createTonemapDrago(1.0, 0.7, 0.85)
    ldrDrago = tonemapDrago.process(hdrDebevec)
    ldrDrago = 3 * ldrDrago    # Multiplied by 3 just because it gave the most pleasing results.
    cv2.imwrite("ldr-Drago.jpg", ldrDrago * 255)
    print("saved ldr-Drago.jpg")

    # Tonemap using Durand's method obtain 24-bit color image
    # createTonemapDurand (gamma,contrast, saturation, sigma_space, sigma_color).
    # sigma_space and sigma_color are the parameters of the bilateral filter that
    # control the amount of smoothing in the spatial and color domains respectively
    tonemapDurand = cv2.createTonemapDurand(1.5,4,1.0,1,1)
    ldrDurand = tonemapDurand.process(hdrDebevec)
    ldrDurand = 3 * ldrDurand
    cv2.imwrite("ldr-Durand.jpg", ldrDurand * 255)
    print("saved ldr-Durand.jpg")

    # Tonemap using Reinhard's method to obtain 24-bit color image
    '''createTonemapReinhard (gamma, intensity, light_adapt, color_adapt)
    The parameter intensity should be in the [-8, 8] range. Greater intensity value
    produces brighter results. light_adapt controls the light adaptation and is in
    the [0, 1] range. A value of 1 indicates adaptation based only on pixel value
    and a value of 0 indicates global adaptation. An in-between value can be used for
    a weighted combination of the two. The parameter color_adapt controls chromatic
    adaptation and is in the [0, 1] range. The channels are treated independently
    if the value is set to 1 and the adaptation level is the same for every channel
    if the value is set to 0. An in-between value can be used for a weighted
    combination of the two.'''
    tonemapReinhard = cv2.createTonemapReinhard(1.5, 0,0,0)
    ldrReinhard = tonemapReinhard.process(hdrDebevec)
    cv2.imwrite("ldr-Reinhard.jpg", ldrReinhard * 255)
    print("saved ldr-Reinhard.jpg")

    # Tonemap using Mantiuk's method to obtain 24-bit color image
    # .createTonemapMantiuk(gamma, scale, saturation);
    # The parameter scale is the contrast scale factor. Values from 0.6 to 0.9 produce best results.
    tonemapMantiuk = cv2.createTonemapMantiuk(2.2,0.85, 1.2)
    ldrMantiuk = tonemapMantiuk.process(hdrDebevec)
    ldrMantiuk = 3 * ldrMantiuk
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
    print (st)
    file_name = st + '.jpg'
    cv2.imwrite(file_name, ldrMantiuk * 255)
    print("saved mantuik:", file_name)
    f = open(file_name, 'rb')
    statinfo = os.stat(file_name)
    status = 0
    headers = {'Content-Type':'application/json'}
    print (file_name)
    # form the http request - see notes above
    payload = '{ "cameraStatus": {"vendor_id": "zeitdice-inc","email_address": "michael@zeitdice.com","fw_version":"123","battery_level":"123","sdSizeMB":"123","sdFreeMB":"123"}, "filename": "%s"}' % (file_name)
    try:
        r = requests.post(host_url, data=payload, headers=headers)
        status = r.status_code
        #logging(String ="Sent Payload")
    except:
        print("ERROR: Could not send payload")

    # check status code was successful
    if status == 201:
        #logging(String ="Got 201 Response")
        data = r.json()
        message = data['message']
        upload_url = data['data']['presigned_url_for_upload']
        print("Server message: %s" % message)
        headers={'Content-Type': 'image/jpeg'}
        print("Uploading: %s" % (file_name))
        start = time.time()
        r = requests.put(upload_url, data=f, headers=headers)
        if r.status_code == 200:
            finish = time.time()
            upload_time = finish-start
            file_size = statinfo.st_size/1024
            print("Success: %s in %s secs (%s KBps)\n" % (file_name,upload_time,(file_size/upload_time)))
            #logging(String ="Uploaded successful")
            os.remove(file_name)
        else:
            print("Error: %s \n" % file_name)
            #logging(String ="Could not upload")
