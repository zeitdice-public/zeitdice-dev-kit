#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
const char* cmdtable[]=
{
  "help",
  "ver",
  "vsys",
  "vbatt",
  "temp",
  "fuel",
  "powersource",
  "mode",
  "interval",
  "uuid",
  "releasepwr",
  "getrtc",
  "setrtc",
  "setinterval",
  "setmaxruntime",
};


void uart_tx (int fd,char* buffer,int size)
{
	if (fd != -1)
		write(fd, buffer,size);		//Filestream, bytes to write, number of bytes to write
}	

void cmd_send(int fd,char* cmd)
{
	if (fd != -1)
		write(fd, cmd,strlen(cmd));		//Filestream, bytes to write, number of bytes to write
}
#define MAX_LEN 256
int main(int argc, char ** argv) {
//-------------------------
  //----- SETUP USART 0 -----
  //-------------------------
  //At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively
  int fd = -1;
  int timeout=0;
  int i, v = 0, size = argc - 1;
  char cmd[256];
  char buffer[MAX_LEN];
     buffer[0] = 0;
     int offset = 0;
     while(argv++,--argc) {
	int toWrite = MAX_LEN-offset;
	int written = snprintf(buffer+offset, toWrite, "%s ", *argv);
	if(toWrite < written) {
	    break;
	}
	offset += written;
     }


     buffer[offset-1]=0;
     sprintf(cmd,"%s\r\n", buffer);
  //OPEN THE UART
  //The flags (defined in fcntl.h):
  //	Access modes (use 1 of these):
  //		O_RDONLY - Open for reading only.
  //		O_RDWR - Open for reading and writing.
  //		O_WRONLY - Open for writing only.
  //
  //	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
  //											if there is no input immediately available (instead of blocking). Likewise, write requests can also return
  //											immediately with a failure status if the output can't be written immediately.
  //
  //	O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become the controlling terminal for the process.
  fd = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);		//Open in non blocking read/write mode
  if (fd == -1)
  {
	  //ERROR - CAN'T OPEN SERIAL PORT
	  printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
  }

  //CONFIGURE THE UART
  //The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
  //	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
  //	CSIZE:- CS5, CS6, CS7, CS8
  //	CLOCAL - Ignore modem status lines
  //	CREAD - Enable receiver
  //	IGNPAR = Ignore characters with parity errors
  //	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
  //	PARENB - Parity enable
  //	PARODD - Odd parity (else even)
  struct termios options;
  tcgetattr(fd, &options);
  options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;		//<Set baud rate
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &options);
  
  
  if (fd != -1)
  { unsigned char rx_buffer[256];
    int length=0;
    int rx_length;
    memset(rx_buffer,0,256);
    timeout=500;
    cmd_send(fd,cmd);
    do{
      rx_length = read(fd, (void*)&rx_buffer[length], 255);
      if(rx_length>0)
      {	length=length+rx_length;
      }
      
      if(length>strlen(cmd))
      {
	if(strstr(&rx_buffer[strlen(cmd)],"\r\n")>0)
	{
	  break;
	}
      }
      usleep(1000);
    }while(timeout--);
    if(strstr(rx_buffer,cmd)>0)
    {
      printf("%s\n",  &rx_buffer[strlen(cmd)]);
    }
   // printf("%08X,%08X\r\n",strstr(rx_buffer,cmd),rx_buffer);
    ///printf("%s\n",  &rx_buffer[strstr(rx_buffer,cmd)]);
  }else
  {	printf("error:fd=%d\r\n",fd);
  }
  
  
  close(fd);

  return 0;
}
