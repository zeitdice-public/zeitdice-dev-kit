#!/bin/bash

echo '================================================================================'
echo '|                                                                              |'
echo '| Hi, let me setup this Raspberry PI as a ZEITDice Dev Kit with 8MP camera module!      |'
echo '|                                                                              |'
echo '================================================================================'

# Setup that is not HW variant dependent
sudo apt-get install ca-certificates -y
sudo apt-get update
sudo apt-get install raspi-config -y
sudo apt-get upgrade -y --fix-missing
sudo apt full-upgrade -y
sudo apt-get dist-upgrade -y --fix-missing
#sudo rpi-update
sudo apt-get --purge remove "x11-*"
sudo apt-get remove python-pip python -y
sudo apt-get install python3-pip python3 -y
sudo apt-get install python3-picamera -y
sudo apt-get install python3-smbus -y
sudo apt-get install python3-dev -y
sudo apt-get install python3-pip -y
sudo apt-get install libhdf5-dev libilmbase-dev webp libopenexr-dev libgstreamer1.0-dev libgtk-3-dev libcblas-dev libatlas-base-dev gfortran libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install ffmpeg -y
sudo apt-get install dnsmasq -y
sudo apt-get install python3-serial -y
sudo apt-get install ntp -y
sudo apt-get install ppp -y
sudo apt-get install wiringpi -y
sudo pip3 install psutil
sudo pip3 install uptime
sudo pip3 install RPi.GPIO
#sudo pip3 install --upgrade RPi.GPIO
sudo pip3 install Pillow
sudo pip3 install pynmea2
sudo pip3 install opencv-contrib-python-headless
sudo pip3 install speedtest-cli
sudo pip3 install matplotlib
sudo pip3 install piexif
sudo apt-get --purge autoremove -y

sudo systemctl stop dnsmasq
sudo update-rc.d -f ntp remove

sudo sync

# Some services can be disabled for fast booting
sudo systemctl disable triggerhappy.service
sudo systemctl disable dnsmasq.service
sudo systemctl disable avahi-daemon.service
sudo systemctl disable hciuart.service
sudo systemctl disable raspi-config.service
sudo systemctl disable keyboard-setup.service
sudo systemctl disable apt-daily.service
# TODO When all this works we can disable dhcpcd and networking as well since operator.py will launch them as needed


sudo sed -i '/dtoverlay=sdtweak,overclock_50/d' /boot/config.txt
sudo sed -i '/disable_splash/d' /boot/config.txt
sudo sed -i '/dboot_delay/d' /boot/config.txt
sudo sed -i '/force_turbo/d' /boot/config.txt
sudo sed -i '/disable_camera_led/d' /boot/config.txt
sudo sed -i '/max_usb_current/d' /boot/config.txt
sudo sed -i '/dtoverlay=pi3-disable-bt/d' /boot/config.txt
sudo sed -i '/start_x/d' /boot/config.txt
sudo sed -i '/gpu_mem/d' /boot/config.txt
sudo sed -i '/dtparam=audio=on/d' /boot/config.txt
#sudo sed -i '/dtparam=i2c_vc/d' /boot/config.txt

sudo sed -i "\$adtoverlay=sdtweak,overclock_50=100" /boot/config.txt
sudo sed -i "\$adisable_splash=1" /boot/config.txt
sudo sed -i "\$aboot_delay=0" /boot/config.txt
sudo sed -i "\$aforce_turbo=1" /boot/config.txt
sudo sed -i "\$adisable_camera_led=1" /boot/config.txt
sudo sed -i "\$amax_usb_current=1" /boot/config.txt
sudo sed -i "\$adtoverlay=pi3-disable-bt" /boot/config.txt
sudo sed -i "\$astart_x=1" /boot/config.txt
sudo sed -i "\$agpu_mem=128" /boot/config.txt #Needs to be 200 for 16MP
sudo sed -i "\$a#dtparam=audio=on" /boot/config.txt
#sudo sed -i "\$adtparam=i2c_vc=on" /boot/config.txt


# Get all config files
sudo mkdir /home/pi/current
sudo mkdir /home/pi/static
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/capture-mode-dhcpcd.txt
sudo mv capture-mode-dhcpcd.txt /home/pi/current/capture-mode-dhcpcd.conf
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/config-mode-dhcpcd.txt
sudo mv config-mode-dhcpcd.txt /home/pi/current/config-mode-dhcpcd.conf

sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/capture-mode-wpa-supplicant.txt
sudo mv capture-mode-wpa-supplicant.txt /home/pi/current/capture-mode-wpa-supplicant.conf
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/config-mode-wpa-supplicant.txt
sudo mv config-mode-wpa-supplicant.txt /home/pi/current/config-mode-wpa-supplicant.conf

sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/dnsmasq.txt
sudo mv dnsmasq.txt /etc/dnsmasq.conf

# Get all files relevant to LTE Connection 
sudo wget --no-check-certificate https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/ppp_installer/chat-connect -O chat-connect
sudo wget --no-check-certificate https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/ppp_installer/chat-disconnect -O chat-disconnect
sudo wget --no-check-certificate https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/ppp_installer/provider -O provider
sudo mv chat-connect /etc/chatscripts/
sudo mv chat-disconnect /etc/chatscripts/
sudo mkdir -p /etc/ppp/peers

# Get operator.py
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/operator.py
sudo mv operator.py /home/pi/current/operator.py
sudo chmod +x /home/pi/current/operator.py

# Get rescuebot.py
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/rescuebot.py
sudo mv rescuebot.py /home/pi/current/rescuebot.py
sudo chmod +x /home/pi/current/rescuebot.py

# Install Updater
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/updater/updater-main.sh
sudo mkdir /home/pi/updater
sudo mv updater-main.sh /home/pi/updater/updater-main.sh
sudo chmod +x /home/pi/updater/updater-main.sh

# Install First App after reboot
sudo mkdir /home/pi/proposed
# need to have FW OTA waiting server side to get picked up and installed by operator after several reboots 
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/apps/master/app.py
sudo mv app.py /home/pi/proposed/app.py
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/apps/master/install-app.sh
sudo mv install-app.sh /home/pi/proposed/install-app.sh
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/apps/master/schedule.wpi
sudo mv schedule.wpi /home/pi/proposed/schedule.wpi


# Install Node to run web interface when in config mode
sudo curl -sL https://deb.nodesource.com/setup_12.x | bash -
sudo apt-get install -y nodejs
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/ap-interface.tar
sudo tar -xvf ap-interface.tar
sudo mv ap-interface /home/pi/current
sudo mkdir /home/pi/current/ap-interface/stream
sudo chmod +x /home/pi/current/ap-interface/stream
sudo chown -R pi:pi /home/pi/current/ap-interface
sudo rm ap-interface.tar

# Set Timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata
# Set Time
sudo ln -fs /usr/share/zoneinfo/UTC /etc/localtime
sudo /etc/init.d/ntp stop
sudo ntpd -q -g
sudo /etc/init.d/ntp start

# Get LTE Installer 
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/ppp_installer/install-lte.sh
sudo chmod +x install-lte.sh

# Get WittyPi and LTE install script, still has to be manually executed after LTE shield is mounted to RPI
sudo wget -N https://gitlab.com/zeitdice-public/zeitdice-dev-kit/raw/master/installWittyPi.sh
sudo chmod +x installWittyPi.sh
#sudo pip3 install wittyPy


echo "Unless you can see any errors, everything should be installed now and you can proceed with the setup procedure. First App will need to get installed via OTA"

