#!/usr/bin/env python3

# Note: Install dependencies:
#       sudo pip3 install uptime

import datetime

max_uptime_minutes = 10.0
seconds_in_minute = 60.0
max_uptime_seconds = int(max_uptime_minutes * seconds_in_minute)
boot_threshold_seconds = 125.0 # about 2 minutes -- takes ~5 seconds to set wittypi

def restart():
  import subprocess
  command = "/usr/bin/sudo /sbin/shutdown -r now"
  process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
  output = process.communicate()[0]
  print (output)

def shutdown():
  import subprocess
  command = "/usr/bin/sudo /sbin/shutdown -h now"
  process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
  output = process.communicate()[0]
  print (output)

def get_current_uptime_seconds():
  from uptime import uptime
  return int(uptime())

def get_next_boot_time():
  import wittyPy
  witty = wittyPy.WittyPi('/home/pi/wittyPi/')
  return witty.next_start

def get_current_time():
  import wittyPy
  witty = wittyPy.WittyPi('/home/pi/wittyPi/')
  return witty.next_start

def time_until_next_boot():
  next_boot = get_next_boot_time()
  return next_boot.t_left()  

def set_next_boot(boot_dt):
  import wittyPy
  witty = wittyPy.WittyPi('/home/pi/wittyPi/')
  witty.set_startup(boot_dt.day, boot_dt.hour, boot_dt.minute, boot_dt.second)

if __name__ == '__main__':
  print("\n{}".format('*'*20))
  print("Starting rescuebot...\n")
 
  current_uptime_seconds = get_current_uptime_seconds()
  print("The system has been up for {} seconds".format(current_uptime_seconds))
  
  if current_uptime_seconds > max_uptime_seconds:
    print("Rebooting as max system uptime is {} seconds.\n".format(max_uptime_seconds)) 

    time_delta_next_boot = time_until_next_boot()
    next_boot_time = get_next_boot_time()
    # current_system_time = datetime.datetime.now()

    print(next_boot_time)

    if time_delta_next_boot < datetime.timedelta(seconds=0):
        print("Need to reset startup time...current boot time delta {}".format(time_delta_next_boot))
        next_boot_datetime = next_boot_time.as_date + (-1 * time_delta_next_boot)  + datetime.timedelta(seconds=boot_threshold_seconds)        
        
        print("The next boot time will be: {}.".format(next_boot_datetime))
        set_next_boot(next_boot_datetime) 

        print('Will now boot in: {}'.format(time_until_next_boot()))
        print(get_next_boot_time())
    else:
        print("Startup time is in the future...")
    
    # restart()
    shutdown()
  else:
    print("No action as max system uptime is {} seconds.".format(max_uptime_seconds))
 
  print("\nEnding rescuebot\n")
  quit() 

