#!/bin/bash

#Not Helpful during DEBUG
sudo mkdir /data
sudo echo "proc                  /proc     proc    defaults             0     0
/dev/mmcblk0p1  /boot     vfat    defaults,ro          0     2
/dev/mmcblk0p2  /         ext4    defaults,noatime,ro  0     1
#/dev/mmcblk0p3  /data        ext4    defaults,noatime,rw  0     1
tmpfs        /tmp            tmpfs   nosuid,nodev         0       0
tmpfs        /var/log        tmpfs   nosuid,nodev         0       0
tmpfs        /var/tmp        tmpfs   nosuid,nodev         0       0" > /etc/fstab
sudo sed -i 's/fastboot noswap ro//g' /boot/cmdline.txt
sudo sed -i '/console/ s/$/ fastboot noswap ro/' /boot/cmdline.txt

sudo rm -rf /var/lib/dhcp /var/lib/dhcpcd5 /var/spool /etc/resolv.conf
sudo ln -s /tmp /var/lib/dhcp
sudo ln -s /tmp /var/lib/dhcpcd5
sudo ln -s /tmp /var/spool
sudo touch /tmp/dhcpcd.resolv.conf
sudo ln -s /tmp/dhcpcd.resolv.conf /etc/resolv.conf
sudo rm /var/lib/systemd/random-seed
sudo ln -s /tmp/random-seed /var/lib/systemd/random-seed
