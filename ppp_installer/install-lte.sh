#!/bin/sh
echo "Installing Sixfab 4G/LTE Base Shield"
echo "What is your carrier APN?"
read carrierapn 

sed -i "s/#APN/$carrierapn/" provider
mv provider /etc/ppp/peers/provider

if ! (grep -q 'route' /etc/ppp/ip-up ); then
    echo "sudo route del default" >> /etc/ppp/ip-up
    echo "sudo route add default ppp0" >> /etc/ppp/ip-up
fi

echo "To connect to internet run \"sudo pon\" and to disconnect run \"sudo poff\" "
echo "You probably want to reboot"
